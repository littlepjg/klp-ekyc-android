package vn.com.opencv.test.ocrapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kalapa.antocr.AntOcr;
import com.kalapa.antocr.AntOcrCallback;
import com.kalapa.antocr.SessionManager;
import com.kalapa.antocr.model.CapturedPicture;
import com.kalapa.antocr.model.CardInfo;
import com.kalapa.antocr.model.FaceVerifyInfo;
import com.kalapa.antocr.model.PictureType;
import com.kalapa.antocr.model.SessionObject;

import java.util.List;

import vn.com.opencv.test.ocrapplication.databinding.BasicFunctionsActivityBinding;

public class BasicFunctionsActivity extends AppCompatActivity {
    CapturedPicture frontCard;
    CapturedPicture backCard;
    CapturedPicture face;
    CapturedPicture temp;
    BasicFunctionsActivityBinding binding;
    private static final String TAG = BasicFunctionsActivity.class.getName();
    private String kalapaProvidedToken = "5bb42ea331ee010001a0b7d71dba5816861b4fa7ad5aadc567fbc5e9";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = BasicFunctionsActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        new SessionManager(kalapaProvidedToken).getSessionToken(this, Config.DEFAULT_PLAN, Config.SHOW_ALL_RESULT, Config.UPLOAD_TYPE, Config.CALLBACK_URL, new AntOcrCallback<SessionObject>() {
            @Override
            public void onOk(SessionObject data) {
                Log.d(TAG, "Generate Session Ok. " + data.getToken());
                AntOcr.init(data.getToken(), SessionManager.getHost());
                AntOcr.setCheckRealFace(false);
            }

            @Override
            public void onNG(Exception e) {
                e.printStackTrace();
            }
        });

        binding.checkFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean checked = ((ToggleButton) view).isChecked();
                AntOcr.setCheckRealFace(checked);
            }
        });

        binding.scanFrontCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frontCard = null;
                AntOcr.startScan(BasicFunctionsActivity.this, PictureType.CARD_FRONT, new AntOcrCallback<List<CapturedPicture>>() {
                    @Override
                    public void onOk(List<CapturedPicture> data) {
                        binding.result.setText(null);
                        StringBuilder result = new StringBuilder();
                        if (data != null && data.size() > 0) {
                            frontCard = data.get(0);
                            result.append(frontCard.toString());
                            result.append("\n");
                        }
                        binding.result.setText(result);
                        binding.ivBackReview.setVisibility(View.GONE);
                        binding.ivSelfieReview.setVisibility(View.GONE);
                        setImageView(binding.ivFrontReview, frontCard);
                    }

                    @Override
                    public void onNG(Exception e) {
                        binding.result.setText(e.getMessage());
                    }
                });
            }
        });

        binding.scanBackCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backCard = null;
                AntOcr.startScan(BasicFunctionsActivity.this, PictureType.CARD_BACK, new AntOcrCallback<List<CapturedPicture>>() {
                    @Override
                    public void onOk(List<CapturedPicture> data) {
                        binding.result.setText(null);
                        StringBuilder result = new StringBuilder();
                        if (data != null && data.size() > 0) {
                            backCard = data.get(0);
                            result.append(backCard.toString());
                            result.append("\n");
                        }
                        binding.result.setText(result);
                        binding.ivFrontReview.setVisibility(View.GONE);
                        binding.ivSelfieReview.setVisibility(View.GONE);
                        binding.ivBackReview.setVisibility(View.VISIBLE);
                        setImageView(binding.ivBackReview, backCard);
                    }

                    @Override
                    public void onNG(Exception e) {
                        binding.result.setText(e.getMessage());
                    }
                });
            }
        });

        binding.scanFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                face = null;
                AntOcr.startScan(BasicFunctionsActivity.this, PictureType.FACE, new AntOcrCallback<List<CapturedPicture>>() {
                    @Override
                    public void onOk(List<CapturedPicture> data) {
                        binding.result.setText(null);
                        StringBuilder result = new StringBuilder();
                        if (data != null && data.size() > 0) {
                            face = data.get(0);
                            result.append(face.toString());
                            result.append("\n");
                        }
                        binding.result.setText(result);
                        binding.ivBackReview.setVisibility(View.GONE);
                        binding.ivFrontReview.setVisibility(View.GONE);
                        setImageView(binding.ivSelfieReview, face);
                    }

                    @Override
                    public void onNG(Exception e) {
                        binding.result.setText(e.getMessage());
                    }
                });
            }
        });


        binding.scanCallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frontCard = null;
                backCard = null;
                face = null;
                AntOcr.startScan(BasicFunctionsActivity.this, new AntOcrCallback<List<CapturedPicture>>() {
                    @Override
                    public void onOk(List<CapturedPicture> data) {
                        binding.result.setText(null);
                        StringBuilder result = new StringBuilder();
                        if (data != null) {
                            for (int i = 0; i < data.size(); i++) {
                                CapturedPicture item = data.get(i);
                                switch (item.type) {
                                    case FACE:
                                        face = item;
                                        setImageView(binding.ivSelfieReview, face);
                                        break;
                                    case CARD_BACK:
                                        backCard = item;
                                        setImageView(binding.ivBackReview, backCard);
                                        break;
                                    case CARD_FRONT:
                                        frontCard = item;
                                        setImageView(binding.ivFrontReview, frontCard);
                                        break;
                                }
                                result.append(item.toString());
                                result.append("\n");
                            }
                        }
                        binding.result.setText(result);
                    }

                    @Override
                    public void onNG(Exception e) {
                        binding.result.setText(e.getMessage());
                    }
                });
            }
        });

        binding.scanWithResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AntOcr.startScanAndDetect(BasicFunctionsActivity.this);
            }
        });

        binding.detectFrontCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.result.setText(null);
                final StringBuilder result = new StringBuilder();
                if (frontCard == null) {
                    Toast.makeText(BasicFunctionsActivity.this, "File not found, please take picture first", Toast.LENGTH_LONG).show();
                    return;
                }
                binding.loadingCard.setVisibility(View.VISIBLE);
                AntOcr.getFrontCard(frontCard.file,
                        new AntOcrCallback<CardInfo>() {
                            @Override
                            public void onOk(CardInfo data) {
                                result.append("============\n");
                                result.append(data.toString());
                                result.append("\n");
                                binding.result.setText(result);
                                binding.loadingCard.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onNG(Exception e) {
                                e.printStackTrace();
                                result.append(e.getMessage());
                                binding.result.setText(result);
                                binding.loadingCard.setVisibility(View.INVISIBLE);
                            }
                        });
            }
        });

        binding.detectBackCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.result.setText(null);
                final StringBuilder result = new StringBuilder();
                if (backCard == null) {
                    Toast.makeText(BasicFunctionsActivity.this, "File not found, please take picture first", Toast.LENGTH_LONG).show();
                    return;
                }
                binding.loadingCard.setVisibility(View.VISIBLE);
                AntOcr.getBackCard(backCard.file,
                        new AntOcrCallback<CardInfo>() {
                            @Override
                            public void onOk(CardInfo data) {
                                result.append("============\n");
                                result.append(data.toString());
                                result.append("\n");
                                binding.result.setText(result);
                                binding.loadingCard.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onNG(Exception e) {
                                result.append(e.getMessage());
                                binding.result.setText(result);
                                binding.loadingCard.setVisibility(View.INVISIBLE);
                            }
                        });
            }
        });

        binding.detectCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.result.setText(null);
                final StringBuilder result = new StringBuilder();

                if (frontCard == null || backCard == null) {
                    Toast.makeText(BasicFunctionsActivity.this, "File not found, please take picture first", Toast.LENGTH_LONG).show();
                    return;
                }
                binding.loadingCard.setVisibility(View.VISIBLE);
                AntOcr.getCardInfo(BasicFunctionsActivity.this, frontCard.file, backCard.file,
                        new AntOcrCallback<CardInfo>() {
                            @Override
                            public void onOk(CardInfo data) {
                                result.append("============\n");
                                result.append(data.toString());
                                result.append("\n");
                                binding.result.setText(result);
                                binding.loadingCard.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onNG(Exception e) {
                                result.append(e.getMessage());
                                binding.result.setText(result);
                                binding.loadingCard.setVisibility(View.INVISIBLE);
                            }
                        }, new AntOcrCallback<CardInfo>() {
                            @Override
                            public void onOk(CardInfo data) {
                                result.append("============\n");
                                result.append(data.toString());
                                result.append("\n");
                                binding.result.setText(result);
                                binding.loadingCard.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onNG(Exception e) {
                                result.append(e.getMessage());
                                binding.result.setText(result);
                                binding.loadingCard.setVisibility(View.INVISIBLE);
                            }
                        });
            }
        });

        binding.verifyFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.result.setText(null);
                if (frontCard == null || face == null) {
                    Toast.makeText(BasicFunctionsActivity.this, "File " + (frontCard == null ? " Front " : " Face ") + "not found, please take picture first", Toast.LENGTH_LONG).show();
                    return;
                }
                binding.loadingCard.setVisibility(View.VISIBLE);
                AntOcr.faceVerify(face.file, new AntOcrCallback<FaceVerifyInfo>() {
                    @Override
                    public void onOk(FaceVerifyInfo data) {
                        binding.result.setText(data.toString());
                        binding.loadingCard.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onNG(Exception e) {
                        binding.result.setText(e.getMessage());
                        binding.loadingCard.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });

    }

    String getCheckFaceMode() {
        if (AntOcr.isCheckRealFace()) return "Check real face";
        return "Take picture face only";
    }

    void setImageView(ImageView imageView, CapturedPicture image) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        imageView.setLayoutParams(params);
        Bitmap cardBitmap = BitmapFactory.decodeFile(image.file.getAbsolutePath());
        Glide.with(BasicFunctionsActivity.this)
                .load(cardBitmap)
                .apply(new RequestOptions())
                .into(imageView);
    }

}
