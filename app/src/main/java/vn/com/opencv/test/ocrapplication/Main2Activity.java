package vn.com.opencv.test.ocrapplication;


import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.LIVENESS_RESULT;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.LIVENESS_URI;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.OCR_BACK_URI;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.OCR_FRONT_URI;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.OCR_RESULT;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.SESSION_PLAN;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kalapa.antocr.AntOcrCallback;
import com.kalapa.antocr.EkycActivity;
import com.kalapa.antocr.EkycActivityResult;
import com.kalapa.antocr.IdCardOcrActivity;
import com.kalapa.antocr.LivenessActivity;
import com.kalapa.antocr.SessionManager;
import com.kalapa.antocr.model.SessionObject;
import com.kalapa.antocr.utils.SDKUtils;

import java.io.IOException;
import java.net.URI;

import vn.com.opencv.test.ocrapplication.databinding.ActivityMain2Binding;

public class Main2Activity extends AppCompatActivity {
    private static final String TAG = Main2Activity.class.getName();
    private static final int LIVENESS_CODE = 101;
    private static final int OCR_CODE = 102;
    private static final int FRONT_SCAN_ONLY_CODE = 103;
    private static final int FULL_FLOW_EKYC_CODE = 104;
    //    private static final String kalapaProvidedToken = "";
    String DEFAULT_PLAN = Config.DEFAULT_PLAN;
    Boolean SHOW_ALL_RESULT = Config.SHOW_ALL_RESULT;
    String UPLOAD_TYPE = Config.UPLOAD_TYPE;
    String CALLBACK_URL = Config.CALLBACK_URL;
    EditText editToken;
    TextView tvLiveResult;
    ImageView ivLiveResult;
    ImageView ivBack;
    ImageView ivFront;
    TextView tvOcrResult;
    private long lastClicked = System.currentTimeMillis();
    String kalapaProvidedToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        setupBinding();
    }


    void setupBinding() {
        Button btnLivenessDetection = findViewById(R.id.btn_liveness_detection);
        Button btnOcrOnly = findViewById(R.id.btn_ocr_only);
        Button btnFullEykc = findViewById(R.id.btn_full_eykc);
        editToken = findViewById(R.id.edit_token);
        tvLiveResult = findViewById(R.id.tv_live_result);
        ivLiveResult = findViewById(R.id.iv_live_result);
        ivBack = findViewById(R.id.iv_back);
        ivFront = findViewById(R.id.iv_front);
        tvOcrResult = findViewById(R.id.tv_ocr_result);
        Button btnResultTest = findViewById(R.id.btn_sample_result);

        btnLivenessDetection.setOnClickListener((v) -> {
            if (shouldClick(1000))
                if (kalapaProvidedToken != null && !kalapaProvidedToken.isEmpty()) {
                    if (kalapaProvidedToken.equals("kalapa@2021")) {
                        kalapaProvidedToken = "5bb42ea331ee010001a0b7d7707457da34b2407b89215fbd375f209d";
                    }
                    new SessionManager(kalapaProvidedToken).getSessionToken(this, DEFAULT_PLAN, SHOW_ALL_RESULT, UPLOAD_TYPE, CALLBACK_URL, new AntOcrCallback<SessionObject>() {
                        @Override
                        public void onOk(SessionObject data) {
                            Log.d(TAG, "Get new Session Ok. " + data.getToken());
                            Intent intent = new Intent(Main2Activity.this, LivenessActivity.class);
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_ID, data.getToken());
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_PLAN, DEFAULT_PLAN);
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_CALLBACK_URL, CALLBACK_URL);
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_UPLOAD_TYPE, UPLOAD_TYPE);
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_SHOW_ALL_RESULT, SHOW_ALL_RESULT);
                            startActivityForResult(intent, LIVENESS_CODE);
                        }

                        @Override
                        public void onNG(Exception e) {
                            Toast.makeText(Main2Activity.this, "Vui lòng nhập vào Token được Kalapa cung cấp hoặc liên hệ để được hỗ trợ", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    });

                } else
                    Toast.makeText(Main2Activity.this, "Vui lòng nhập vào Token được Kalapa cung cấp", Toast.LENGTH_LONG).show();

        });

        btnOcrOnly.setOnClickListener((v) -> {
            if (shouldClick(1000)) {
                Log.d(TAG, "Enter clicked");
                if (kalapaProvidedToken != null && !kalapaProvidedToken.isEmpty()) {
                    if (kalapaProvidedToken.equals("kalapa@2021")) {
                        kalapaProvidedToken = "5bb42ea331ee010001a0b7d7707457da34b2407b89215fbd375f209d";
                    }
                    Log.d(TAG, kalapaProvidedToken);
                    Intent intent = new Intent(Main2Activity.this, IdCardOcrActivity.class);
                    intent.putExtra(SDKUtils.SDK_CONFIG.TOKEN, kalapaProvidedToken);
                    intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_PLAN, DEFAULT_PLAN);
                    intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_CALLBACK_URL, CALLBACK_URL);
                    intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_UPLOAD_TYPE, UPLOAD_TYPE);
                    intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_SHOW_ALL_RESULT, SHOW_ALL_RESULT);
                    startActivityForResult(intent, OCR_CODE);
                } else
                    Toast.makeText(Main2Activity.this, "Vui lòng nhập vào Token được Kalapa cung cấp", Toast.LENGTH_LONG).show();
            }
        });

        btnFullEykc.setOnClickListener((v) -> {
            if (kalapaProvidedToken != null && !kalapaProvidedToken.isEmpty()) {
                if (shouldClick(3000)) {
                    Toast.makeText(Main2Activity.this, "Đang kết nối tới hệ thống, vui lòng đợi giây lát...", Toast.LENGTH_LONG).show();
                    if (kalapaProvidedToken.equals("kalapa@2021")) {
                        kalapaProvidedToken = "5bb42ea331ee010001a0b7d7707457da34b2407b89215fbd375f209d";
                    }
                    new SessionManager(kalapaProvidedToken).getSessionToken(this, DEFAULT_PLAN, SHOW_ALL_RESULT, UPLOAD_TYPE, CALLBACK_URL, new AntOcrCallback<SessionObject>() {
                        @Override
                        public void onOk(SessionObject data) {
                            Log.d(TAG, "Get new Session Ok. " + data.getToken());
                            Log.d(TAG, data.getToken());
                            Intent intent = new Intent(Main2Activity.this, EkycActivity.class);
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_ID, data.getToken());
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_PLAN, DEFAULT_PLAN);
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_CALLBACK_URL, CALLBACK_URL);
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_UPLOAD_TYPE, UPLOAD_TYPE);
                            intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_SHOW_ALL_RESULT, SHOW_ALL_RESULT);
                            if (shouldClick(1000))
                                startActivityForResult(intent, FULL_FLOW_EKYC_CODE);
                        }

                        @Override
                        public void onNG(Exception e) {
                            // Get Token Problem
                            e.printStackTrace();
                            Toast.makeText(Main2Activity.this, "Xảy ra lỗi khi lấy Token, vui lòng cung cấp đúng Token đã được cung cấp bởi Kalapa", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else
                Toast.makeText(Main2Activity.this, "Vui lòng nhập vào Token được Kalapa cung cấp", Toast.LENGTH_LONG).show();
        });

        btnResultTest.setOnClickListener((v) -> {
            if (shouldClick(1000)) {
                Log.d(TAG, "Clicked Activity Result");
                Intent intent = new Intent(Main2Activity.this, EkycActivityResult.class);
                intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_ID, "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaWF0IjoxNjM0Nzg0MjIyLCJleHAiOjE2MzQ3ODQ4MjJ9.Yp858isc5ggl7LhE9IANIKUnzcDv2aMUNJYmtdhERuat2Sk9wEzUZtphDPacpdcQ5zvtb2Xra1lyS5FN2qqHUg");
                //eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaWF0IjoxNjM0Nzg0MjIyLCJleHAiOjE2MzQ3ODQ4MjJ9.Yp858isc5ggl7LhE9IANIKUnzcDv2aMUNJYmtdhERuat2Sk9wEzUZtphDPacpdcQ5zvtb2Xra1lyS5FN2qqHUg
                //eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaWF0IjoxNjM0MDA1NzMyLCJleHAiOjE2MzQwMDYzMzJ9.jOKe9aPHWkQoSwvlxbpMmhwioeTd5vDeXG2Kw9BjIuuqKNrtUIQVkX989FcjJSrcZguD4h5GdiWUUQOlubkV3A
                startActivityForResult(intent, 123);
            }
        });
        editToken.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                kalapaProvidedToken = s.toString();
                Log.d(TAG, "On Text Change: " + kalapaProvidedToken);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private boolean shouldClick(int timeout) {
        if (System.currentTimeMillis() - lastClicked > timeout) {
            lastClicked = System.currentTimeMillis();
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, " Result Code: " + resultCode);
        if (requestCode == LIVENESS_CODE) {
            handleLivenessData(resultCode, data);
        } else if (requestCode == OCR_CODE) {
            handleOcrData(resultCode, data);
        }
    }

    private void handleLivenessData(int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (data != null) {
                String selfieUri = data.getStringExtra(LIVENESS_URI);
                String selfieData = data.getStringExtra(LIVENESS_RESULT);
                Log.d(TAG, "Selfie Data: " + selfieData);
                runOnUiThread(() -> {
                    if (selfieUri != null) {
                        ivLiveResult.setImageBitmap(BitmapFactory.decodeFile(selfieUri));
                        ivLiveResult.setVisibility(View.VISIBLE);
                    }
                    tvLiveResult.setText(selfieData);
                });
            }
        } else {
            if (data != null) {
                String selfieData = data.getStringExtra(LIVENESS_RESULT);
                ivLiveResult.setVisibility(View.GONE);
                tvLiveResult.setText(selfieData);
            }
        }
    }

    private void handleOcrData(int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (data != null) {
                String frontUri = data.getStringExtra(OCR_FRONT_URI);
                String backUri = data.getStringExtra(OCR_BACK_URI);
                String ocrData = data.getStringExtra(OCR_RESULT);
                Log.d(TAG, "OCR Data: " + ocrData);
                runOnUiThread(() -> {
                    if (frontUri != null) {
                        ivFront.setImageBitmap(BitmapFactory.decodeFile(frontUri));
                        ivFront.setVisibility(View.VISIBLE);
                    }
                    if (backUri != null) {
                        ivBack.setImageBitmap(BitmapFactory.decodeFile(backUri));
                        ivBack.setVisibility(View.VISIBLE);
                    }
                    tvOcrResult.setText(ocrData);
                });
            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this.getApplicationContext(), "Không có kết nối mạng hoặc xảy ra lỗi khi thao tác, vui lòng thử lại sau", Toast.LENGTH_SHORT).show();
        } else {
            if (data != null) {
                String ocrData = data.getStringExtra(OCR_RESULT);
                ivFront.setVisibility(View.GONE);
                ivBack.setVisibility(View.GONE);
                tvOcrResult.setText(ocrData);
            }
        }
    }
}
