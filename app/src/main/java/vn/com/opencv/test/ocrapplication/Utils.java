package vn.com.opencv.test.ocrapplication;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

class Utils {
    public Bitmap getBase64FromPath(String photoPath) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap orgBitmap = BitmapFactory.decodeFile(photoPath, options);

        Bitmap scaleBitmap = getResizedBitmap(orgBitmap, 300);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        scaleBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        Bitmap decodedByte = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

//        Toast.makeText(Main2Activity.this, "orgBitmap size = " + sizeOf(orgBitmap) + " bytes", Toast.LENGTH_LONG).show();
//        Toast.makeText(Main2Activity.this, "bitmap size = " + sizeOf(scaleBitmap) + " bytes", Toast.LENGTH_LONG).show();

        String base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return decodedByte;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newMaxLength) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        long newArea = (long) newMaxLength * newMaxLength;
        long oldArea = (long) width * height;
        if (newArea < oldArea) {
            float scale = (float) Math.sqrt((double) newArea / oldArea);
            // float scaleWidth = ((float) newWidth) / width;
            // float scaleHeight = ((float) newHeight) / height;

            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scale, scale);
            // RECREATE THE NEW BITMAP
            return Bitmap.createBitmap(bm, 0, 0, width, height,
                    matrix, false);
        } else {
            return bm;
        }
    }

    public static Bitmap getBitmapFromFile(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        return BitmapFactory.decodeFile(path, options);
    }

    public static String getOrgBase64FromPath(String path) {
        String base64 = "";
        try {
            File file = new File(path);
            byte[] buffer = new byte[(int) file.length() + 100];
            @SuppressWarnings("resource")
            int length = new FileInputStream(file).read(buffer);
            base64 = Base64.encodeToString(buffer, 0, length,
                    Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return base64;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    protected int sizeOf(Bitmap data) {
        if (data == null) {
            return 0;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }
}