package vn.com.opencv.test.ocrapplication;

public class Config {
    public static final String DEFAULT_PLAN = "PLAN_FULL";
    public static final boolean SHOW_ALL_RESULT = true;
    public static final String CALLBACK_URL = "https://dev-tung.kalapa.vn";
    public static final String UPLOAD_TYPE = "WEBRTC";
}
