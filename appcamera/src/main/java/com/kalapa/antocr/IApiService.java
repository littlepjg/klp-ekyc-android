package com.kalapa.antocr;

import com.kalapa.antocr.model.*;

import io.reactivex.*;
import okhttp3.*;
import retrofit2.http.*;

interface IApiService {

    @Multipart
    @POST("/api/kyc/check-selfie")
    Observable<FaceVerifyInfo> faceVerify(
            @Header("Authorization") String apiKey,
            @Part("request_id") RequestBody requestId,
            @Part MultipartBody.Part image);

    @Multipart
    @POST("/api/kyc/check-front/")
    Observable<CardInfo> detectFront(
            @Header("Authorization") String apiKey,
            @Part("request_id") RequestBody requestId,
            @Part MultipartBody.Part image);

    @POST("/api/auth/get-token/")
    Observable<SessionObject> getSessionToken(
            @Body GetTokenObj getTokenObj);

    @GET("/api/kyc/ping/")
    Observable<Object> checkIfSessionExpired(
            @Header("Authorization") String app_token);

    @GET("/api/data/get")
    Observable<CardInfo> getIdData(
            @Header("Authorization") String app_token,
            @Query("type") String type);

    @GET("/api/data/get?type=SELFIE")
    Observable<FaceVerifyInfo> getSelfieData(
            @Header("Authorization") String app_token);

    @GET("/api/data/get?type=VERIFY")
    Observable<DataVerify> getVerifyData(
            @Header("Authorization") String app_token);

    @GET("/api/data/get?type=LOGIC")
    Observable<VerifyOCR> getLogicData(
            @Header("Authorization") String app_token);

    @GET("/api/data/get?type=DECISION_DETAIL")
    Observable<DecisionSummarize> getDecisionData(
            @Header("Authorization") String app_token);

    @GET("/api/data/get?type=ANTIFRAUD")
    Observable<AntifraudFull> getAntifraudData(
            @Header("Authorization") String app_token);


    @Multipart
    @POST("/api/kyc/check-back/")
    Observable<CardInfo> detectBack(
            @Header("Authorization") String apiKey,
            @Part("request_id") RequestBody requestId,
            @Part MultipartBody.Part image);


}
