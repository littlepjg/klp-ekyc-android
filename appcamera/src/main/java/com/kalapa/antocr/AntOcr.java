package com.kalapa.antocr;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;


import com.kalapa.antocr.model.AntifraudFull;
import com.kalapa.antocr.model.BaseObserver;
import com.kalapa.antocr.model.CapturedPicture;
import com.kalapa.antocr.model.CardInfo;
import com.kalapa.antocr.model.CountryCode;
import com.kalapa.antocr.model.DataVerify;
import com.kalapa.antocr.model.DecisionSummarize;
import com.kalapa.antocr.model.FaceVerifyInfo;
import com.kalapa.antocr.model.PictureType;
import com.kalapa.antocr.model.VerifyOCR;
import com.kalapa.antocr.utils.Common;
import com.kalapa.antocr.utils.FileUtils;
import com.kalapa.antocr.utils.PermissionUtils;

import java.io.File;
import java.nio.file.LinkPermission;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class AntOcr {
    private static ApiService apiService;
    private static boolean checkRealFace = false;
    static AntOcrCallback<List<CapturedPicture>> pictureCallback;
    static AntOcrCallback<List<CapturedPicture>> otherCallback;
    private static String tag = AntOcr.class.getSimpleName();
    private static String _sessionId = null;
    private static CountryCode countryCode = CountryCode.VN;

    public static void init(String apiKey, String host) {
        apiService = new ApiService(apiKey, host);
    }

    public static boolean isCheckRealFace() {
        return checkRealFace;
    }

    public static void setCheckRealFace(boolean checkRealFace) {
        AntOcr.checkRealFace = checkRealFace;
    }

    public static void startScan(Context context, final AntOcrCallback<List<CapturedPicture>> callback) {
        pictureCallback = callback;
        Intent intent = new Intent(context, CardScanningActivity.class);
        context.startActivity(intent);
    }

    public static void startScanAndDetect(Context context) {
        pictureCallback = null;
        Intent intent = new Intent(context, CardScanningActivity.class);
        context.startActivity(intent);
    }

    public static void getCardInfo(Context context, File font, File back, final AntOcrCallback<CardInfo> fontCallback, final AntOcrCallback<CardInfo> backCallback) {
        if (Common.isNetworkAvailable(context)) {
            getFrontCard(font, fontCallback);
            getBackCard(back, backCallback);
        } else {
            Toast.makeText(context.getApplicationContext(), "Vui lòng kiểm tra lại kết nối internet của bạn", Toast.LENGTH_SHORT).show();
        }
    }

    public static void getFrontCard(File card, final AntOcrCallback<CardInfo> callback) {
        Observable<CardInfo> detectObserver;
        detectObserver = apiService.detectFront(card);
        detectObserver
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<CardInfo>() {

                    @Override
                    public void onNext(CardInfo cardInfo) {
                        if (callback != null) {
                            callback.onOk(cardInfo);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (callback != null) {
                            callback.onNG(new Exception(e.getMessage()));
                        }
                    }
                });
    }

    public static void getAntifraudFull(final AntOcrCallback<AntifraudFull> callback) {
        Observable<AntifraudFull> detectObserver;
        detectObserver = apiService.getAntifraudData();
        detectObserver
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<AntifraudFull>() {
                    @Override
                    public void onNext(AntifraudFull antifraudFull) {
                        if (callback != null) {
                            callback.onOk(antifraudFull);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (callback != null) {
                            callback.onNG(new Exception(e.getMessage()));
                        }
                    }
                });
    }

    public static void getVerifyData(final AntOcrCallback<DataVerify> callback) {
        Observable<DataVerify> detectObserver;
        detectObserver = apiService.getVerifyData();
        detectObserver
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<DataVerify>() {
                    @Override
                    public void onNext(DataVerify data) {
                        if (callback != null) {
                            callback.onOk(data);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (callback != null) {
                            callback.onNG(new Exception(e.getMessage()));
                        }
                    }
                });
    }

    public static void getDecisionDetail(final AntOcrCallback<DecisionSummarize> callback) {
        Observable<DecisionSummarize> detectObserver;
        detectObserver = apiService.getDecisionData();
        detectObserver
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<DecisionSummarize>() {
                    @Override
                    public void onNext(DecisionSummarize data) {
                        if (callback != null) {
                            callback.onOk(data);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (callback != null) {
                            callback.onNG(new Exception(e.getMessage()));
                        }
                    }
                });
    }

    public static void getFaceVerify(final AntOcrCallback<FaceVerifyInfo> callback) {
        Observable<FaceVerifyInfo> detectObserver;
        detectObserver = apiService.getSelfieData();
        detectObserver
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<FaceVerifyInfo>() {
                    @Override
                    public void onNext(FaceVerifyInfo data) {
                        if (callback != null) {
                            callback.onOk(data);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (callback != null) {
                            callback.onNG(new Exception(e.getMessage()));
                        }
                    }
                });
    }


    public static void getBackCard(File card, final AntOcrCallback<CardInfo> callback) {
        Observable<CardInfo> detectObserver;
        detectObserver = apiService.detectBack(card);
        detectObserver
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<CardInfo>() {

                    @Override
                    public void onNext(CardInfo cardInfo) {
                        if (callback != null) {
                            callback.onOk(cardInfo);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (callback != null) {
                            callback.onNG(new Exception(e.getMessage()));
                        }
                    }
                });
    }

    public static void faceVerify(File face, final AntOcrCallback<FaceVerifyInfo> callback) {
//        apiService.faceVerify(face)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new BaseObserver<FaceVerifyInfo>() {
//                    @Override
//                    public void onNext(FaceVerifyInfo cardInfo) {
//                        if (callback != null) {
//                            callback.onOk(cardInfo);
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        e.printStackTrace();
//                        if (callback != null) {
//                            callback.onNG(new Exception(e.getMessage()));
//                        }
//                    }
//                });
    }

    public static void startScan(Context context, PictureType type, final AntOcrCallback<List<CapturedPicture>> callback) {
        pictureCallback = callback;
        Log.d(tag, "startScan " + type);
        if (type != null) {
            Intent intent = new Intent(context, SingleScanningActivity.class);
            intent.putExtra("type", type);
            context.startActivity(intent);
        } else {
            Intent intent = new Intent(context, CardScanningActivity.class);
            context.startActivity(intent);
        }
    }

    public static void startLiveness(Context context) {
        Intent intent = new Intent(context, LivenessActivity.class);
        context.startActivity(intent);
    }

    public static String getSession() {
        if (TextUtils.isEmpty(_sessionId)) {
            _sessionId = UUID.randomUUID().toString();
        }
        Log.d(tag, "getSession: " + _sessionId);
        return _sessionId;
    }

    public static String startNewSession(Context context) {
        Log.d(tag, "startNewSession");
        deleteOldFile(context);
        _sessionId = null;
        return getSession();
    }

    private static void deleteOldFile(Context context) {
        FileUtils.getCardBackFile(context).deleteOnExit();
        FileUtils.getCardFile(context).deleteOnExit();
        FileUtils.getFaceFile(context).deleteOnExit();
        FileUtils.getFaceFileCloseEyes(context).deleteOnExit();
        FileUtils.getFaceFileLeft(context).deleteOnExit();
        FileUtils.getFaceFileRight(context).deleteOnExit();
    }

    public static CountryCode getCountryCode() {
        return countryCode;
    }

    public static void setCountryCode(CountryCode countryCode) {
        AntOcr.countryCode = countryCode;
    }
}