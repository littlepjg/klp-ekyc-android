/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kalapa.antocr;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.kalapa.antocr.model.BaseObserver;
import com.kalapa.antocr.model.CapturedPicture;
import com.kalapa.antocr.model.PictureType;
import com.kalapa.antocr.utils.BitmapUtil;
import com.kalapa.antocr.utils.FileUtils;
import com.otaliastudios.cameraview.PictureResult;

import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class CardCroppingActivity extends AppCompatActivity {
    private Bitmap imageCapturedBitmap;
    public static final int REQUEST_CODE = 1805;
    private final boolean canRetake = false;
    private PublishSubject<byte[]> _cropTask = PublishSubject.create();

    int widthScreen;
    int heightScreen;
    boolean isFront = true;
    //boolean isFace = false;
    private boolean mCapturingPicture;
    private boolean mCapturingVideo;
    private boolean mCanNext = false;
    private boolean isTakedFront = false;
    private boolean isTakedBack = false;
    // private boolean isTakeFace = false;
    // To show stuff in the callback
    private long mCaptureTime;

    CameraView camera;
    View frame;
    ImageView takePicture;
    Button next;
    TextView tvTake;
    View loadingview;
    View imageCapturedHolder;
    Button btConfirm;
    TextView tvTitle;
    Button btRetake;
    ImageView imageCaptured;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.card_scanning_activity);
        initLayout();

//        camera.mapGesture(Gesture.TAP, GestureAction.FOCUS); // Tap to focus!

        camera.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                super.onPictureTaken(result);
                Log.d("taint", "onImage");
                _cropTask.onNext(result.getData());
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCanNext) {

                    openFaceDetect();
                }
            }
        });

        if (canRetake) {
            next.setVisibility(View.VISIBLE);
        } else {
            next.setVisibility(View.INVISIBLE);
        }

        handlerImageCaptured();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frame.getLayoutParams();
        float dp40 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());
        layoutParams.width = (int) (getResources().getDisplayMetrics().widthPixels - dp40);
        layoutParams.height = getResources().getDisplayMetrics().heightPixels / 3;
        frame.setLayoutParams(layoutParams);

        widthScreen = getResources().getDisplayMetrics().widthPixels;
        heightScreen = getResources().getDisplayMetrics().heightPixels;

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCapturingPicture) return;
                mCapturingPicture = true;
                mCaptureTime = System.currentTimeMillis();
                if (imageCapturedBitmap == null) {
//                    camera.capturePicture();
                    camera.takePicture();
                    loadingview.setVisibility(View.VISIBLE);
                } else {
                    imageCapturedHolder.setVisibility(View.GONE);
                }

            }
        });
        imageCapturedHolder.setVisibility(View.GONE);

        btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageCapturedHolder.setVisibility(View.GONE);
                takePicture.setVisibility(View.VISIBLE);
                openFaceDetect();
            }
        });

        frame.setBackgroundDrawable(ContextCompat.getDrawable(CardCroppingActivity.this, R.drawable.identity_front_frame));
        tvTitle.setText(R.string.ocr_confirm_photo_mess_front);
        btRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isTakedFront && !isTakedBack) {
                    isFront = true;
                    isTakedFront = false;
                    frame.setBackgroundDrawable(ContextCompat.getDrawable(CardCroppingActivity.this, R.drawable.identity_front_frame));
                    tvTitle.setText(R.string.ocr_confirm_photo_mess_front);
                } else {
                    isFront = false;
                    isTakedBack = false;
                    frame.setBackgroundDrawable(ContextCompat.getDrawable(CardCroppingActivity.this, R.drawable.identity_back_frame));
                    tvTitle.setText(R.string.ocr_confirm_photo_mess_back);
                }
                imageCapturedHolder.setVisibility(View.GONE);
                takePicture.setVisibility(View.VISIBLE);

            }
        });
        requestOptions = new RequestOptions();
        requestOptions = requestOptions.transform(new CenterCrop(), new RoundedCorners(16));
    }

    private void initLayout() {
        camera = findViewById(R.id.camera);
        frame = findViewById(R.id.frame);
        takePicture = findViewById(R.id.takePicture);
        next = findViewById(R.id.next);
        tvTake = findViewById(R.id.tv_take);
        loadingview = findViewById(R.id.loadingview);
        imageCapturedHolder = findViewById(R.id.imageCapturedHolder);
        btConfirm = findViewById(R.id.bt_confirm);
        tvTitle = findViewById(R.id.tv_title);
        btRetake = findViewById(R.id.bt_retake);
        imageCaptured = findViewById(R.id.imageCaptured);
    }

    RequestOptions requestOptions;

    private void openFaceDetect() {
        if (!isTakedFront || !isTakedBack) {
            if (isTakedFront) {
                frame.setBackgroundDrawable(ContextCompat.getDrawable(CardCroppingActivity.this, R.drawable.identity_back_frame));
                tvTitle.setText(R.string.ocr_confirm_photo_mess_back);
            } else if (isTakedBack) {
                frame.setVisibility(View.INVISIBLE);
                tvTitle.setText(R.string.ocr_confirm_face_mess);
                camera.toggleFacing();

            }
            return;
        } else {
            startFaceActivity();
        }
        isFront = true;
        mCanNext = false;
        isTakedBack = false;
        isTakedFront = false;
        frame.setVisibility(View.VISIBLE);
        frame.setBackgroundDrawable(ContextCompat.getDrawable(CardCroppingActivity.this, R.drawable.identity_front_frame));
        tvTitle.setText(R.string.ocr_confirm_photo_mess_front);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                if (AntOcr.pictureCallback != null) {
                    genResult();
                    finish();
                } else {
                    Intent intent = new Intent(CardCroppingActivity.this, CropResultActivity.class);
                    startActivity(intent);
                }
            } else {
                showAlertDialog();
            }
        }
    }

    public void startFaceActivity() {
        if (AntOcr.isCheckRealFace()) {
            Intent intent = new Intent(CardCroppingActivity.this, FaceTrackerActivity.class);
            startActivityForResult(intent, 100);
        } else {
            Intent intent = new Intent(CardCroppingActivity.this, FaceCaptureActivity.class);
            startActivityForResult(intent, 100);
        }
    }

    public void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Thông báo");
        builder.setMessage("Không xác nhận được khuôn mặt. Bạn có muốn thử lại không.");
        builder.setCancelable(false);
        builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startFaceActivity();
            }
        });
        builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                genResultError();
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void genResultError() {
        if (AntOcr.pictureCallback != null) {
            AntOcr.pictureCallback.onNG(new Exception("Không nhận diện được khuôn mặt"));
        }
    }

    private void genResult() {
        ArrayList<CapturedPicture> images = new ArrayList<>();
        images.add(new CapturedPicture(
                FileUtils.getCardFile(CardCroppingActivity.this),
                PictureType.CARD_FRONT
        ));
        images.add(new CapturedPicture(
                FileUtils.getCardBackFile(CardCroppingActivity.this),
                PictureType.CARD_BACK
        ));
        images.add(new CapturedPicture(
                FileUtils.getFaceFile(CardCroppingActivity.this),
                PictureType.FACE
        ));
        if (AntOcr.pictureCallback != null) {
            AntOcr.pictureCallback.onOk(images);
        }
    }

    private void handlerImageCaptured() {
        _cropTask.subscribeOn(Schedulers.computation())
                .map(new Function<byte[], Bitmap>() {
                    @Override
                    public Bitmap apply(byte[] capturedImage) throws IOException {
                        Bitmap bitmap = CameraUtils.decodeBitmap(capturedImage);
//                        Bitmap bitmap = CameraUtils.decodeBitmap(capturedImage, 800, 800, new BitmapFactory.Options());
                        if (isFront) {
                            FileUtils.saveFile(CardCroppingActivity.this, bitmap, FileUtils.cardFileName);
                        } else {
                            FileUtils.saveFile(CardCroppingActivity.this, bitmap, FileUtils.cardBackFileName);
                        }
                        return bitmap;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<Bitmap>() {
                    @Override
                    public void onNext(Bitmap bitmap) {
                        super.onNext(bitmap);
                        loadingview.setVisibility(View.GONE);
                        if (isFront) {
                            imageCapturedHolder.setVisibility(View.VISIBLE);
                            takePicture.setVisibility(View.GONE);
                            frame.setBackgroundDrawable(ContextCompat.getDrawable(CardCroppingActivity.this, R.drawable.identity_front_frame));
                            tvTitle.setText(R.string.ocr_confirm_photo_mess_front);
                            checkTakeCMTComplete(1);
                            isTakedFront = true;
                            isFront = false;
                        } else {
                            checkTakeCMTComplete(2);
                            imageCapturedHolder.setVisibility(View.VISIBLE);
                            takePicture.setVisibility(View.GONE);
                            frame.setBackgroundDrawable(ContextCompat.getDrawable(CardCroppingActivity.this, R.drawable.identity_back_frame));
                            tvTitle.setText(R.string.ocr_confirm_photo_mess_back);
                            isTakedBack = true;
                        }
                        Glide.with(CardCroppingActivity.this).load(bitmap).apply(requestOptions)
                                .into(imageCaptured);
                        mCapturingPicture = false;
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mCapturingPicture = false;

                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mCapturingPicture = false;

                    }
                });
    }

    private void checkTakeCMTComplete(int type) {

//        switch (type){
//            case 1:{
//                if(isTakedBack){
//                    tvTake.setText(getString(R.string.ocr_take_complete));
//                    next.setVisibility(View.VISIBLE);
//                }else
//                    tvTake.setText(getString(R.string.ocr_take_back));
//            }break;
//            case 2:{
//                if(isTakedFront){
//                    tvTake.setText(getString(R.string.ocr_take_complete));
//                    next.setVisibility(View.VISIBLE);
//                }else
//                    tvTake.setText(getString(R.string.ocr_take_front));
//            }break;
//        }
    }

    private void clearImage() {
        imageCaptured.setImageBitmap(null);
        if (imageCapturedBitmap != null) {
            imageCapturedBitmap.recycle();
            imageCapturedBitmap = null;
        }
    }

    private Bitmap cropBitmap(Bitmap srcBmp) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frame.getLayoutParams();

        int widthBitmap = srcBmp.getWidth();
        int heightBitmap = srcBmp.getHeight();

        float ratio = (float) layoutParams.width / (float) widthScreen;

        int width = (int) (widthBitmap * ratio);
        int height = (int) ((float) layoutParams.height / (float) layoutParams.width * (float) width);

        int x = (int) ((float) (widthBitmap - width) / 2.0);
        int y = (int) ((float) (heightBitmap - height) / 2.0);

        Bitmap bitmap = Bitmap.createBitmap(
                srcBmp,
                x,
                y,
                width,
                height
        );
        Bitmap scaled = BitmapUtil.resizedImage(bitmap, 800.0, 800.0);
        if (scaled != bitmap) {
            bitmap.recycle();
        }
        return scaled;
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermissionCamera();
    }

    @Override
    protected void onPause() {
        if(camera.isTakingVideo()){
            camera.stopVideo();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        camera.destroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showSettingsPermission() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Thông báo");
        builder.setMessage("Ứng dụng chưa được cấp quyền sử dụng camera, nhấn vào cài đặt để tiếp tục");
        builder.setCancelable(false);
        builder.setPositiveButton("Cài đặt", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private boolean checkPermissionCamera() {
//        new SlackRestClient(SlackRestClient.SLACK_WEBHOOKS_DEV_TU).sendMessage("Permission Camera Check Called");
//        new SlackRestQueue(SlackRestQueue.SLACK_WEBHOOKS_DEV_TU,"Permission Camera Check Called").execute();
//        Log.d("DEV_TUNG","PERMISSION CAMERA CHECK");
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        222);
            } else {
                showSettingsPermission();
            }
            return false;
        } else {
            camera.setLifecycleOwner(this);
            return true;
        }
    }
}
