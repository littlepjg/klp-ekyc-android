package com.kalapa.antocr;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.kalapa.antocr.handlers.CheckSessionExpiredHttpHandler;
import com.kalapa.antocr.model.BaseObserver;
import com.kalapa.antocr.model.SessionObject;
import com.kalapa.antocr.utils.Common;

import java.util.concurrent.ExecutionException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import lombok.Data;

@Data
public class SessionManager {
    private static ApiService apiService;
    private static SessionManager mInstance;
    private SessionObject currSession;
    private String token;
    private static final String host = "https://dev-tung.kalapa.vn/";

    public SessionManager(String token) {
        this.token = token;
    }

    public static String getHost() {
        return host;
    }

    public static boolean checkIfSessionExpired(Context context, String sessionId) {
        try {
            return new CheckSessionExpiredHttpHandler(context).execute(sessionId).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void getSessionToken(Context context, String plan, boolean showAllResult, String uploadType, String callbackUrl, final AntOcrCallback<SessionObject> callback) {
        if (!Common.isNetworkAvailable(context)) {
            Toast.makeText(context.getApplicationContext(), "Không có kết nối internet, vui lòng kiểm tra lại trước khi tiếp tục", Toast.LENGTH_SHORT).show();
            return;
        }
        Observable<SessionObject> sessionObserver;
        apiService = new ApiService(null, host);
        sessionObserver = apiService.getSessionToken(token, plan, showAllResult, uploadType, callbackUrl);
        sessionObserver
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<SessionObject>() {
                    @Override
                    public void onNext(SessionObject sessionObject) {
                        if (callback != null) {
                            callback.onOk(sessionObject);
                            currSession = sessionObject;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (callback != null) {
                            callback.onNG(new Exception(e.getMessage()));
                        }
                    }
                });
    }
}
