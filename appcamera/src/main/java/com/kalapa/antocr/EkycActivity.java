package com.kalapa.antocr;

import static com.kalapa.antocr.SessionManager.checkIfSessionExpired;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.kalapa.antocr.databinding.ActivityEkycBinding;
import com.kalapa.antocr.databinding.ActivityOcrIdCardBinding;
import com.kalapa.antocr.model.CapturedPicture;
import com.kalapa.antocr.model.CardInfo;
import com.kalapa.antocr.model.PictureType;
import com.kalapa.antocr.model.SessionObject;
import com.kalapa.antocr.utils.SDKUtils;

import java.io.File;
import java.util.List;


public class EkycActivity extends AppCompatActivity {
    ActivityEkycBinding binding;
    private static final String TAG = EkycActivity.class.getName();
    String idCardOcrResult;
    int idCardOcrCode;
    int intentResult;
    String sessionObject;
    CardInfo cardInfo;
    CardInfo frontInfo;
    CardInfo backInfo;
    String frontUrl;
    String backUrl;
    String selfieUrl;
    String livenessMessage;
    int livenessResultCode;
    private static final int LIVENESS_CODE = 101;
    private static final int LIVENESS_RESULT = 111;
    private long lastClick = System.currentTimeMillis();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEkycBinding.inflate(getLayoutInflater());
        if (getSupportActionBar() != null) getSupportActionBar().hide();
        setContentView(binding.getRoot());
        binding.btnTakeFront.setOnClickListener((v) ->
                Toast.makeText(EkycActivity.this, "Vui lòng chờ hệ thống đang kết nối", Toast.LENGTH_LONG).show()
        );
        binding.btnTakeBack.setOnClickListener((v) ->
                Toast.makeText(EkycActivity.this, "Vui lòng chờ hệ thống đang kết nối", Toast.LENGTH_LONG).show()
        );
        onPreExecute();
        onUIUpdate();
    }

    private boolean isClickable() {
        if (System.currentTimeMillis() - lastClick > 1000) {
            lastClick = System.currentTimeMillis();
            return true;
        }
        return false;
    }

    private void updateIntent(String key, String value) {
        Intent i = getIntent();
        i.putExtra(key, value);
    }

    private void onExecute() {
        AntOcr.init(sessionObject, SessionManager.getHost());

        binding.btnTakeFront.setOnClickListener((v) -> {
            takeFront();
        });

        binding.btnTakeBack.setOnClickListener((v) -> {
            takeBack();
        });


        binding.btnContinue.setOnClickListener((v) -> {
            shouldContinue();
        });
    }

    private void takeBack() {
        if (isClickable())
            AntOcr.startScan(EkycActivity.this, PictureType.CARD_BACK, new AntOcrCallback<List<CapturedPicture>>() {
                @Override
                public void onOk(List<CapturedPicture> data) {
                    if (data != null && data.size() > 0) {
                        backUrl = data.get(0).getFile().getAbsolutePath();
                        updateIntent(SDKUtils.SDK_CONFIG.OCR_BACK_URI, backUrl);
                        binding.ivBack.setImageBitmap(BitmapFactory.decodeFile(backUrl));
                        binding.tvResultBack.setText("Đang bóc tách thông tin, vui lòng chờ...");
                        // Call get id
                        runOnUiThread(() -> {
                            if (frontUrl == null)
                                takeFront();
                        });
                        onBackDataProcess(backUrl);

                    } else {
                        backUrl = null;
                        updateIntent(SDKUtils.SDK_CONFIG.OCR_BACK_URI, backUrl);
                        binding.tvResultBack.setText("Capture error, no image has been captured");
                    }
                }

                @Override
                public void onNG(Exception e) {
                    binding.tvResultBack.setText(e.getMessage());
                    idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR;
                    idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResult = RESULT_CANCELED;
                    backUrl = null;
                    updateIntent(SDKUtils.SDK_CONFIG.OCR_BACK_URI, backUrl);
                    preDestroy();
                }
            });
    }

    private void shouldContinue() {
        if (frontUrl != null && backUrl != null
                && frontInfo != null
                && frontInfo.getError() != null
                && frontInfo.getError().getCode() == 0
                && frontInfo.getData() != null
                && frontInfo.getData().getCard_type() != null
                && backInfo != null
                && backInfo.getError() != null
                && backInfo.getError().getCode() == 0
                && backInfo.getData() != null
                && backInfo.getData().getCard_type() != null
        ) {
            Log.d(TAG, "Front: " + frontInfo);
            Log.d(TAG, "Back: " + backInfo);
            String frontCard = frontInfo.getData().getCard_type().split("_")[0];
            String backCard = backInfo.getData().getCard_type().split("_")[0];
            if (frontCard.trim().equals(backCard.trim())) {
                binding.btnContinue.setVisibility(View.VISIBLE);
                if (isClickable())
                    onMoveToStep2();
            } else {
                Log.d(TAG, "Different Type of Card: " + frontCard + " " + backCard);
                binding.tvResultFront.setText("Vui lòng chụp cùng loại giấy tờ");
                binding.tvResultBack.setText("Vui lòng chụp cùng loại giấy tờ");
                binding.btnContinue.setVisibility(View.GONE);
                // Set invisible
            }
        } else {
            Log.d(TAG, "Error...");
            Toast.makeText(EkycActivity.this, "Vui lòng chụp ảnh hợp lệ trước khi tiếp tục", Toast.LENGTH_LONG).show();
            binding.btnContinue.setVisibility(View.GONE);
        }
    }

    private void takeFront() {
        if (isClickable())
            AntOcr.startScan(EkycActivity.this, PictureType.CARD_FRONT, new AntOcrCallback<List<CapturedPicture>>() {
                @Override
                public void onOk(List<CapturedPicture> data) {
                    if (data != null && data.size() > 0) {
                        frontUrl = data.get(0).getFile().getAbsolutePath();
                        updateIntent(SDKUtils.SDK_CONFIG.OCR_FRONT_URI, frontUrl);
                        binding.ivFront.setImageBitmap(BitmapFactory.decodeFile(frontUrl));
                        binding.tvResultFront.setText("Đang bóc tách thông tin, vui lòng chờ...");
                        // Call get id
                        runOnUiThread(() -> {
                            if (backUrl == null) {
                                takeBack();
                            }
                        });
                        onFrontDataProcess(frontUrl);
                    } else {
                        frontUrl = null;
                        updateIntent(SDKUtils.SDK_CONFIG.OCR_FRONT_URI, frontUrl);
                        binding.tvResultFront.setText("Capture error, no image has been captured");
                    }
                }

                @Override
                public void onNG(Exception e) {
                    binding.tvResultFront.setText(e.getMessage());
                    backUrl = null;
                    updateIntent(SDKUtils.SDK_CONFIG.OCR_BACK_URI, backUrl);
                }
            });
    }

    private void onMoveToStep2() {
        Intent intent = new Intent(EkycActivity.this, EkycActivityStep2.class);
        intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_ID, sessionObject);
        intent.putExtra(SDKUtils.SDK_CONFIG.OCR_FRONT_URI, frontUrl);
        intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT, idCardOcrResult);
        startActivityForResult(intent, LIVENESS_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "On Activity Result: " + resultCode + " - RequestCode" + requestCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LIVENESS_CODE) {
            if (resultCode == RESULT_OK) {
                // Xử lý tiếp
                if (data != null) {
                    selfieUrl = data.getStringExtra(SDKUtils.SDK_CONFIG.LIVENESS_URI);
                    livenessMessage = data.getStringExtra(SDKUtils.SDK_CONFIG.LIVENESS_RESULT);
                    livenessResultCode = data.getIntExtra(SDKUtils.SDK_CONFIG.LIVENESS_RESULT_CODE, 99);
                    // Show Card Info, và Selfie here.
                    Intent intent = new Intent(EkycActivity.this, EkycActivityResult.class);
                    intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_ID, sessionObject);
                    intent.putExtra(SDKUtils.SDK_CONFIG.LIVENESS_URI, selfieUrl);
                    intent.putExtra(SDKUtils.SDK_CONFIG.OCR_FRONT_URI, frontUrl);
                    intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT, idCardOcrResult);
                    Log.d(TAG, "Calling end result");
                    startActivityForResult(intent, LIVENESS_RESULT);
                }
            } else {
                Toast.makeText(EkycActivity.this, "Xảy ra lỗi ở bước Selfie, Vui lòng thử lại trước khi tiếp tục", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == LIVENESS_RESULT) {
            Intent intent = getIntent();
            intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT_CODE, intent.getStringExtra(SDKUtils.SDK_CONFIG.OCR_RESULT_CODE));
            intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT, intent.getStringExtra(SDKUtils.SDK_CONFIG.OCR_RESULT));
            intent.putExtra(SDKUtils.SDK_CONFIG.EKYC_SELFIE_RESULT, intent.getStringExtra(SDKUtils.SDK_CONFIG.EKYC_SELFIE_RESULT));
            intent.putExtra(SDKUtils.SDK_CONFIG.EKYC_VERIFY_RESULT, intent.getStringExtra(SDKUtils.SDK_CONFIG.EKYC_VERIFY_RESULT));
            intent.putExtra(SDKUtils.SDK_CONFIG.EKYC_DECISION_RESULT, intent.getStringExtra(SDKUtils.SDK_CONFIG.EKYC_DECISION_RESULT));
            intent.putExtra(SDKUtils.SDK_CONFIG.EKYC_ANTIFRAUD_RESULT, intent.getStringExtra(SDKUtils.SDK_CONFIG.EKYC_ANTIFRAUD_RESULT));
            setResult(resultCode, intent);
            finish();
        }
    }

    private void onFrontDataProcess(String frontUri) {
        File file = new File(frontUri);
        frontInfo = null;
        if (file.exists()) {
            AntOcr.getFrontCard(file, new AntOcrCallback<CardInfo>() {
                @Override
                public void onOk(CardInfo data) {
                    // Check message
                    // Check type
                    frontInfo = data;
                    if (data != null) {
                        if (data.getError().getCode() == 0) {
                            // Vào việc
                            binding.tvResultFront.setText("Ảnh hợp lệ");
                            binding.tvResultFront.setTextColor(getResources().getColor(R.color.ekyc_green));
                            if (cardInfo == null)
                                cardInfo = data;
                            else {
                                // Set info
                                try {
                                    cardInfo.mergeInformation(data.getData());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            idCardOcrResult = new Gson().toJson(cardInfo);
                        } else {
                            assert data.getError() != null;
                            binding.tvResultFront.setText(data.getError().getMessage());
                            binding.tvResultFront.setTextColor(getResources().getColor(R.color.ekyc_red));
                        }
                    } else {
                        // Không có data . rarely
                        idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR;
                        idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                        intentResult = RESULT_CANCELED;
                        preDestroy();
                    }
                    onUIUpdate();
                }

                @Override
                public void onNG(Exception e) {
                    idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR + e.getMessage();
                    idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResult = RESULT_CANCELED;
                    preDestroy();
                }
            });
        }
    }


    private void onBackDataProcess(String backUri) {
        File file = new File(backUri);
        backInfo = null;
        if (file.exists()) {
            AntOcr.getBackCard(file, new AntOcrCallback<CardInfo>() {
                @Override
                public void onOk(CardInfo data) {
                    // Check message
                    // Check type
                    backInfo = data;
                    if (data != null) {
                        if (data.getError().getCode() == 0) {
                            // Vào việc
                            binding.tvResultBack.setText("Ảnh hợp lệ");
                            binding.tvResultBack.setTextColor(getResources().getColor(R.color.ekyc_green));
                            if (cardInfo == null)
                                cardInfo = data;
                            else {
                                // Set info
                                try {
                                    cardInfo.mergeInformation(data.getData());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            idCardOcrResult = new Gson().toJson(cardInfo);
                        } else {
                            assert data.getError() != null;
                            binding.tvResultBack.setText(data.getError().getMessage());
                            binding.tvResultBack.setTextColor(getResources().getColor(R.color.ekyc_red));
                        }
                    } else {
                        // Không có data . rarely
                        idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR;
                        idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                        intentResult = RESULT_CANCELED;
                        preDestroy();
                    }
                    onUIUpdate();
                }

                @Override
                public void onNG(Exception e) {
                    idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR + e.getMessage();
                    idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResult = RESULT_CANCELED;
                    preDestroy();
                }
            });
        }
    }

    private void onUIUpdate() {
        if (frontUrl != null && backUrl != null
                && frontInfo != null
                && frontInfo.getError() != null
                && frontInfo.getError().getCode() == 0
                && frontInfo.getData() != null
                && frontInfo.getData().getCard_type() != null
                && backInfo != null
                && backInfo.getError() != null
                && backInfo.getError().getCode() == 0
                && backInfo.getData() != null
                && backInfo.getData().getCard_type() != null
        ) {
            Log.d(TAG, "Front: " + frontInfo);
            Log.d(TAG, "Back: " + backInfo);
            String frontCard = frontInfo.getData().getCard_type().split("_")[0];
            String backCard = backInfo.getData().getCard_type().split("_")[0];
            if (frontCard.trim().equals(backCard.trim())) {
                binding.btnContinue.setVisibility(View.VISIBLE);
            }
        } else {
            binding.btnContinue.setVisibility(View.INVISIBLE);
        }
    }


    private void onPreExecute() {
        Intent intent = getIntent();
        String token = intent.getStringExtra(SDKUtils.SDK_CONFIG.TOKEN);
        String session = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_ID);
        String DEFAULT_PLAN = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_PLAN);
        String UPLOAD_TYPE = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_UPLOAD_TYPE);
        Boolean SHOW_ALL_RESULT = intent.getBooleanExtra(SDKUtils.SDK_CONFIG.SESSION_SHOW_ALL_RESULT, false);
        String CALLBACK_URL = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_CALLBACK_URL);
        if (session != null) {
            if (checkIfSessionExpired(EkycActivity.this, session)) {
                Log.d(TAG, "Old Session is still  Ok. " + session);
                sessionObject = session;
                onExecute();
            } else {
                // Session is expired
                Log.d(TAG, "Old Session is expired. " + session);
                Toast.makeText(EkycActivity.this, "Phiên làm việc " + session + " đã kết thúc hoặc có lỗi xảy ra, vui lòng thử lại sau...", Toast.LENGTH_LONG).show();
                idCardOcrResult = SDKUtils.ERROR_CODE.SESSION_EXPIRED;
                idCardOcrCode = SDKUtils.OCR_CODE.EXPIRED;
                intentResult = RESULT_CANCELED;
                preDestroy();
            }
        } else {
            new SessionManager(token).getSessionToken(this, DEFAULT_PLAN, SHOW_ALL_RESULT, UPLOAD_TYPE, CALLBACK_URL, new AntOcrCallback<SessionObject>() {
                @Override
                public void onOk(SessionObject data) {
                    Log.d(TAG, "Get new Session Ok. " + data.getToken());
                    sessionObject = data.getToken();
                    onExecute();
                }

                @Override
                public void onNG(Exception e) {
                    // Get Token Problem
                    e.printStackTrace();
                    idCardOcrResult = SDKUtils.ERROR_CODE.NETWORK_ERROR;
                    idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResult = RESULT_CANCELED;
                    preDestroy();
                }
            });
        }

    }


    private void preDestroy() {
        Intent intent = getIntent();
        intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT_CODE, idCardOcrCode);
        intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT, idCardOcrResult);
        setResult(intentResult, intent);
        finish();
    }
}
