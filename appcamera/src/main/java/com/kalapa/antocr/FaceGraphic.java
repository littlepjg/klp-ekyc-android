/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kalapa.antocr;

import android.graphics.*;
import android.util.Log;

import com.kalapa.antocr.ui.camera.*;
import com.google.android.gms.vision.face.*;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
class FaceGraphic extends GraphicOverlay.Graphic {
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float BOX_STROKE_WIDTH = 5.0f;
    private int state = 0;
    private static final int COLOR_CHOICES[] = {
            Color.BLUE,
            Color.CYAN,
            Color.GREEN,
            Color.MAGENTA,
            Color.RED,
            Color.WHITE,
            Color.YELLOW
    };
    private static int mCurrentColorIndex = 0;

    private Paint mFacePositionPaint;
    private Paint mIdPaint;
    private Paint mBoxPaint;

    private volatile Face mFace;
    private int mFaceId;
    private float mFaceHappiness;
    private boolean mIsFrontFacing;
    FaceTrackerActivity faceTrackerActivity;

    FaceGraphic(GraphicOverlay overlay, boolean mIsFrontFacing, FaceTrackerActivity faceTrackerActivity) {
        super(overlay);

        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        mFacePositionPaint = new Paint();
        mFacePositionPaint.setColor(selectedColor);

        mIdPaint = new Paint();
        mIdPaint.setColor(selectedColor);
        mIdPaint.setTextSize(ID_TEXT_SIZE);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(selectedColor);
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
        this.mIsFrontFacing = mIsFrontFacing;
        this.faceTrackerActivity = faceTrackerActivity;
    }

    void setId(int id) {
        mFaceId = id;
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateFace(Face face) {
        mFace = face;
        postInvalidate();
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        Face face = mFace;
        if (face == null) {
            return;
        }

        Log.d("log","sate: ==========:"+ faceTrackerActivity.mState);
        switch (faceTrackerActivity.mState) {

            case 0: {//detect face
                faceTrackerActivity.delayRun(1);
            }
            break;
            case 1: {//close you eye
                if (face.getIsRightEyeOpenProbability() < 0.1 && face.getIsLeftEyeOpenProbability() < 0.1) {
                    faceTrackerActivity.delayRun(-1);
                }
            }break;
            case 2: {//Nghieng dau sang phải
                if (face.getEulerY() < -25) {
                    faceTrackerActivity.delayRun(-1);
                }
            }break;
            case 3: {//Nghieng dau sang trai
                if (face.getEulerY() > 25) {
                    faceTrackerActivity.delayRun(-1);
                }
            }break;


        }

        //
        // Draws a circle at the position of the detected face, with the face's track id below.
//        float x = translateX(face.getPosition().x + face.getWidth() / 2);
//        float y = translateY(face.getPosition().y + face.getHeight() / 2);
////        int i = 0;
////        for (Landmark landmark : face.getLandmarks()) {
////            i++;
////
////
////            float cx = translateX(landmark.getPosition().x);
////            float cy = translateY(landmark.getPosition().y);
////
////            // canvas.drawCircle(cx, cy, 10, rectPaint);
////            String type = String.valueOf(landmark.getType());
////            mIdPaint.setTextSize(50);
////            canvas.drawText(type, cx, cy, mIdPaint);
////        }
//        // Draws a bounding box around the face.
//        float xOffset = scaleX(face.getWidth() / 2.0f);
//        float yOffset = scaleY(face.getHeight() / 2.0f);
//        float left = x - xOffset;
//        float top = y - yOffset;
//        float right = x + xOffset;
//        float bottom = y + yOffset;
//        canvas.drawRect(left, top, right, bottom, mBoxPaint);
    }
}
