package com.kalapa.antocr;

/**
 * Demo_module
 * Created by taint on 06/12/2019.
 */
public interface AntOcrCallback<T> {
    void onOk(T data);
    void onNG(Exception e);
}
