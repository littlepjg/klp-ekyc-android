/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kalapa.antocr;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kalapa.antocr.model.BaseObserver;
import com.kalapa.antocr.model.CapturedPicture;
import com.kalapa.antocr.model.PictureType;
import com.kalapa.antocr.utils.FileUtils;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

//import com.otaliastudios.cameraview.Gesture;
//import com.otaliastudios.cameraview.GestureAction;

public class TakingPictureOnlyActivity extends AppCompatActivity {
    private PublishSubject<byte[]> _cropTask = PublishSubject.create();

    int widthScreen;
    int heightScreen;
    private boolean mCapturingPicture;

    CameraView camera;
    View frame;
    ImageView takePicture;
    TextView tvTake;
    View loadingview;
    View imageCapturedHolder;
    Button btConfirm;
    TextView tvTitle;
    Button btRetake;
    ImageView imageCaptured;
    private String tag = TakingPictureOnlyActivity.class.getSimpleName();
    PictureType runMode;
    float widthRatio = 1.0f;
    float heightRatio = 1.0f;
    RequestOptions requestOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.other_scanning_activity);
        runMode = (PictureType) getIntent().getSerializableExtra("type");
        widthRatio = getIntent().getExtras().getFloat("width_ratio");
        heightRatio = getIntent().getExtras().getFloat("height_ratio");
        Log.d(tag, "DEBUG " + runMode + widthRatio + heightRatio);
        initLayout();
        frame.setBackgroundDrawable(ContextCompat.getDrawable(TakingPictureOnlyActivity.this, R.drawable.rectangle_frame));

        camera.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                super.onPictureTaken(result);
                Log.d(tag, "onPictureTaken");
                _cropTask.onNext(result.getData());
            }
        });
        handlerImageCaptured();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) frame.getLayoutParams();
        float dp40 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics());

        widthScreen = getResources().getDisplayMetrics().widthPixels;
        heightScreen = getResources().getDisplayMetrics().heightPixels;
        // Horizontal
        layoutParams.width = (int) (widthScreen * widthRatio);
        layoutParams.height = (int) (heightScreen * heightRatio);
        frame.setLayoutParams(layoutParams);

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCapturingPicture) return;
                mCapturingPicture = true;
                camera.takePicture();
                loadingview.setVisibility(View.VISIBLE);
            }
        });
        imageCapturedHolder.setVisibility(View.GONE);

        btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageCapturedHolder.setVisibility(View.GONE);
                takePicture.setVisibility(View.VISIBLE);
                finish();
                genResult();

            }
        });

        btRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageCapturedHolder.setVisibility(View.GONE);
                takePicture.setVisibility(View.VISIBLE);
            }
        });
        requestOptions = new RequestOptions();
        requestOptions = requestOptions.transform(new CenterCrop(), new RoundedCorners(16));
    }

    private void initLayout() {
        camera = findViewById(R.id.camera);
        frame = findViewById(R.id.frame);
        takePicture = findViewById(R.id.takePicture);
        tvTake = findViewById(R.id.tv_take);
        loadingview = findViewById(R.id.loadingview);
        imageCapturedHolder = findViewById(R.id.imageCapturedHolder);
        btConfirm = findViewById(R.id.bt_confirm);
        tvTitle = findViewById(R.id.tv_title);
        btRetake = findViewById(R.id.bt_retake);
        imageCaptured = findViewById(R.id.imageCaptured);
        tvTitle.setText(R.string.ocr_confirm_photo_mess_general);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                if (AntOcr.otherCallback != null) {
                    genResult();
                }
            }
            finish();
        }
    }

    private void genResult() {
        ArrayList<CapturedPicture> images = new ArrayList<>();
        images.add(new CapturedPicture(
                FileUtils.getTempFile(TakingPictureOnlyActivity.this),
                PictureType.OTHER
        ));

        if (AntOcr.otherCallback != null) {
            AntOcr.otherCallback.onOk(images);
        }
    }

    private void handlerImageCaptured() {
        _cropTask.subscribeOn(Schedulers.computation())
                .map(new Function<byte[], Bitmap>() {
                    @Override
                    public Bitmap apply(byte[] capturedImage) {
                        Bitmap bitmap = CameraUtils.decodeBitmap(capturedImage, Math.round(1024* widthRatio), Math.round(1024*heightRatio), new BitmapFactory.Options());
                        FileUtils.saveFile(TakingPictureOnlyActivity.this, bitmap, FileUtils.tempFileName);
                        return bitmap;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<Bitmap>() {
                    @Override
                    public void onNext(Bitmap bitmap) {
                        super.onNext(bitmap);
                        loadingview.setVisibility(View.GONE);
                        imageCapturedHolder.setVisibility(View.VISIBLE);
                        takePicture.setVisibility(View.GONE);
                        tvTake.setText(getString(R.string.ocr_confirm_photo_mess_general));
                        // Set preview

                        Glide.with(TakingPictureOnlyActivity.this).load(bitmap).apply(requestOptions)
                                .into(imageCaptured);
                        mCapturingPicture = false;
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mCapturingPicture = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mCapturingPicture = false;
                    }
                });
    }


    @Override
    protected void onResume() {
        Log.d(tag, "onResume");
        super.onResume();
        checkPermissionCamera();
    }


    @Override
    protected void onPause() {
//        camera.stop();
        Log.d(tag, "on Pause");
        if (camera != null)
            camera.stopVideo();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(tag, "on Destroy");
        super.onDestroy();
        if (camera != null)
            camera.destroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(tag, "onRequestPermissionsResult");
    }

    private void showSettingsPermission() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Thông báo");
        builder.setMessage("Ứng dụng chưa được cấp quyền sử dụng camera, nhấn vào cài đặt để tiếp tục");
        builder.setCancelable(false);
        builder.setPositiveButton("Cài đặt", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private boolean checkPermissionCamera() {
//        new SlackRestClient(SlackRestClient.SLACK_WEBHOOKS_DEV_TU).sendMessage("Permission Camera Check Called");
//        new SlackRestQueue(SlackRestQueue.SLACK_WEBHOOKS_DEV_TU,"Permission Camera Check Called").execute();
//        Log.d("DEV_TUNG","PERMISSION CAMERA CHECK");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Log.d(tag, "Permission Not Granted");
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 222);
            } else {
                showSettingsPermission();
            }
            return false;
        } else {
            Log.d(tag, "Permission Granted");
            camera.setLifecycleOwner(this);
            return true;
        }
    }

}
