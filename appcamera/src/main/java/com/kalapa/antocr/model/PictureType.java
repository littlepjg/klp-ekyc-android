package com.kalapa.antocr.model;

public enum PictureType {
    CARD_FRONT,
    CARD_BACK,
    FACE,
    OTHER
}
