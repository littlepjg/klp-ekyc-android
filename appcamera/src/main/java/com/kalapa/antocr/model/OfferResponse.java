package com.kalapa.antocr.model;

public class OfferResponse {
    String sdp;
    String type;

    public OfferResponse() {
    }

    public OfferResponse(String sdp, String type) {
        this.sdp = sdp;
        this.type = type;
    }

    public String getSdp() {
        return sdp;
    }

    public void setSdp(String sdp) {
        this.sdp = sdp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
