package com.kalapa.antocr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class KycPicture {
    private Long id;
    private String session;
    private String client_id;
    private String user_agent;
    private String ip;
    private String front;
    private String id_card_number;
    private String front_data;

    private String verify_data;

    private String antifraud_data;
    private String back;
    private String back_data;
    private String selfie;
    private String selfie_data;
    private Date created_date;
    private Date updated_date;

    private String liveness_data;
    private String liveness_result;
    private String ocr_logic_check;
}
