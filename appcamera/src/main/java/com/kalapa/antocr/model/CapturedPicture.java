package com.kalapa.antocr.model;

import java.io.File;

public class CapturedPicture {
    public File file;
    public PictureType type;

    public CapturedPicture(File file, PictureType type) {
        this.file = file;
        this.type = type;
    }

    @Override
    public String toString() {
        return "CapturedPicture{" +
                "file=" + file +
                ", type=" + type +
                '}';
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public PictureType getType() {
        return type;
    }

    public void setType(PictureType type) {
        this.type = type;
    }
}

