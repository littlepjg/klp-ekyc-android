package com.kalapa.antocr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetTokenObj {
    private String app_token;
    private String session_plan;
    private boolean allow_sdk_full_results;
    private String upload_type;
    private String callback_url;
}
