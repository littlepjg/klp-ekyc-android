package com.kalapa.antocr.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OfferRequest {
    String session_id;
    String type;
    String sdp;
    String video_transform;
    Boolean is_save_result;
    String version;
    Boolean use_additional_check;
}
