package com.kalapa.antocr.model;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class ApiInfo {

    @SerializedName("error_code")
    private String error_code;

    @SerializedName("error_message")
    private String error_message;

}
