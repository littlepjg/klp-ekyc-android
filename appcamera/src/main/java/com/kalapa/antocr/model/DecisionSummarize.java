package com.kalapa.antocr.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DecisionSummarize {
    String decision;
    List<DecisionDetailObj> details;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class DecisionDetailObj {
        private String code;
        private Integer isPass;
        private String info;
        private Date created_date;
        private Date updated_date;
        private String description_en;
        private String description_vi;
    }

}
