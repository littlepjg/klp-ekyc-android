package com.kalapa.antocr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DataVerify {
    IdCardInfo idCardInfo;
    Boolean verified;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class IdCardInfo {
        String id;
        String name;
    }
}
