package com.kalapa.antocr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardInfo {
    Error error;
    MyData data;

    public void mergeInformation(MyData cardInfo) {
        if (data.fields.birthday.isEmpty() && !cardInfo.fields.birthday.isEmpty())
            data.fields.birthday = cardInfo.fields.birthday;
        if (data.fields.doe.isEmpty() && !cardInfo.fields.doe.isEmpty())
            data.fields.doe = cardInfo.fields.doe;
        if (data.fields.doi.isEmpty() && !cardInfo.fields.doi.isEmpty())
            data.fields.doi = cardInfo.fields.doi;
        if (data.fields.ethnicity.isEmpty() && !cardInfo.fields.ethnicity.isEmpty())
            data.fields.ethnicity = cardInfo.fields.ethnicity;
        if (data.fields.features.isEmpty() && !cardInfo.fields.features.isEmpty())
            data.fields.features = cardInfo.fields.features;
        if (data.fields.gender.isEmpty() && !cardInfo.fields.gender.isEmpty())
            data.fields.gender = cardInfo.fields.gender;
        if (data.fields.home.isEmpty() && !cardInfo.fields.home.isEmpty())
            data.fields.home = cardInfo.fields.home;
        if (data.fields.id_number.isEmpty() && !cardInfo.fields.id_number.isEmpty())
            data.fields.id_number = cardInfo.fields.id_number;
        if (data.fields.name.isEmpty() && !cardInfo.fields.name.isEmpty())
            data.fields.name = cardInfo.fields.name;
        if (data.fields.poi.isEmpty() && !cardInfo.fields.poi.isEmpty())
            data.fields.poi = cardInfo.fields.poi;
        if (data.fields.religion.isEmpty() && !cardInfo.fields.religion.isEmpty())
            data.fields.religion = cardInfo.fields.religion;
        if (data.fields.resident.isEmpty() && !cardInfo.fields.resident.isEmpty())
            data.fields.resident = cardInfo.fields.resident;
        if (data.fields.type.isEmpty() && !cardInfo.fields.type.isEmpty())
            data.fields.type = cardInfo.fields.type;
        if (data.fields.national.isEmpty() && !cardInfo.fields.national.isEmpty())
            data.fields.national = cardInfo.fields.national;
        if (data.fields.country.isEmpty() && !cardInfo.fields.country.isEmpty())
            data.fields.country = cardInfo.fields.country;
        if (data.fields.home_entities == null) {
            data.fields.home_entities = cardInfo.fields.home_entities;
        } else if (cardInfo.fields.home_entities != null) {
            if (data.fields.home_entities.district.isEmpty() && !cardInfo.fields.home_entities.district.isEmpty())
                data.fields.home_entities.district = cardInfo.fields.home_entities.district;
            if (data.fields.home_entities.province.isEmpty() && !cardInfo.fields.home_entities.province.isEmpty())
                data.fields.home_entities.province = cardInfo.fields.home_entities.province;
            if (data.fields.home_entities.ward.isEmpty() && !cardInfo.fields.home_entities.ward.isEmpty())
                data.fields.home_entities.ward = cardInfo.fields.home_entities.ward;
        }
        if (data.fields.resident_entities == null) {
            data.fields.resident_entities = cardInfo.fields.resident_entities;
        } else if (cardInfo.fields.resident_entities != null) {
            if (data.fields.resident_entities.district.isEmpty() && !cardInfo.fields.resident_entities.district.isEmpty())
                data.fields.resident_entities.district = cardInfo.fields.resident_entities.district;
            if (data.fields.resident_entities.province.isEmpty() && !cardInfo.fields.resident_entities.province.isEmpty())
                data.fields.resident_entities.province = cardInfo.fields.resident_entities.province;
            if (data.fields.resident_entities.ward.isEmpty() && !cardInfo.fields.resident_entities.ward.isEmpty())
                data.fields.resident_entities.ward = cardInfo.fields.resident_entities.ward;
            if (data.fields.resident_entities.unknown.isEmpty() && !cardInfo.fields.resident_entities.unknown.isEmpty())
                data.fields.resident_entities.unknown = cardInfo.fields.resident_entities.unknown;
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MyData {
        Fields fields;
        Float blur_score;
        String card_type;
        Float flash_score;
        Float photocopy_score;

//        PointObj[] idcard_corners;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Fields {
            String birthday = "";
            String doe = "";
            String doi = "";
            String ethnicity = "";
            String features = "";
            String gender = "";
            String home = "";
            String id_number = "";
            String name = "";
            String poi = "";
            String religion = "";
            String resident = "";
            String type = "";
            String national = "";
            String country = "";
            HomeEntity home_entities;
            ResidenceEntity resident_entities;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class HomeEntity {
            String district;
            String province;
            String ward;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class ResidenceEntity {
            String district;
            String province;
            String ward;
            String unknown;
        }

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Error {
        Integer code = 0;
        String message = "";
    }


}
