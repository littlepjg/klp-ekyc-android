package com.kalapa.antocr.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FaceVerifyRequest {
    File card;
    List<File> faces = new ArrayList<>();
    boolean check3RandomPose;
    boolean check3StraightPose;
    boolean returnFeature;

    public File getCard() {

        return card;
    }

    public void setCard(File card) {
        this.card = card;
    }

    public List<File> getFaces() {
        return faces;
    }

    public void setFaces(List<File> faces) {
        this.faces = faces;
    }

    public boolean getCheck3RandomPose() {
        return check3RandomPose;
    }

    public void setCheck3RandomPose(boolean check3RandomPose) {
        this.check3RandomPose = check3RandomPose;
    }

    public boolean getCheck3StraightPose() {
        return check3StraightPose;
    }

    public void setCheck3StraightPose(boolean check3StraightPose) {
        this.check3StraightPose = check3StraightPose;
    }

    public boolean getReturnFeature() {
        return returnFeature;
    }

    public void setReturnFeature(boolean returnFeature) {
        this.returnFeature = returnFeature;
    }
}
