package com.kalapa.antocr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemView {
    private String label;
    private String value;
    private Boolean isGood;

    public ItemView(String label, String value) {
        this.label = label;
        this.value = value;
    }
}
