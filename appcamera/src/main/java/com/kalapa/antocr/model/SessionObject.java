package com.kalapa.antocr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionObject {
    String token;
    String username;
    String[] roles;
    String current_plan;
    String upload_type;
    Boolean allow_sdk_full_results;
}
