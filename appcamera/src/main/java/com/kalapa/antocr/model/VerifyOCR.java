package com.kalapa.antocr.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class VerifyOCR {
    String Message;
    MyData data;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public class MyData {
        Element validate_province;
        Element validate_gender;
        Element validate_yob;
        Element validate_address;
        Element validate_register_age;
        Element validate_expiry;
        Element validate_dob;

        @AllArgsConstructor
        @NoArgsConstructor
        @Data
        public class Element {
            Message message;
            Boolean value;

            @AllArgsConstructor
            @NoArgsConstructor
            @Data
            public class Message {
                String vi;
                String en;
            }
        }
    }
}
