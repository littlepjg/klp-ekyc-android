package com.kalapa.antocr.model;

import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//**
//{"data":{"anomaly_score":8.584890165366232E-4,
//        "euler_angles":[13.412020130818386,-7.446179552062243,-0.8105533933627281],
//        "face_bbox":[90,124,338,468],
//        "face_keypoints":[[159,245],[280,246],[219,321],[169,388],[264,388]],
//        "is_matched":true,
//        "matching_score":86,
//        "rotating_angle":0,
//        "spam_score":0.026233913376927376},
//        "error":{"code":0,
//        "message":""}}
// *//

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FaceVerifyInfo {
    MyData data;
    Error error;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class MyData {
        Float anomaly_score;
        Boolean is_matched;
        Integer matching_score;
        Integer rotating_angle;
        Float spam_score;
//        Object face_keypoints;
//        Object face_bbox;
//        Object euler_angles;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class Error {
        private Integer code;
        private String message;
    }
}
