package com.kalapa.antocr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AntifraudFull {
    String message;
    Result results;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class Result {
        Boolean blacklist;
        FacesMatched faces_matched;
        Mark mark;
        Recapture recapture;
        AbnormalFace abnormal_face;
        AbnormalText abnormal_text;
        CornerCut corner_cut;
        IdFont id_font;

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public class FacesMatched {
            Integer count;
//            Object[] list;
        }

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public class Mark {
            Boolean prediction;
            Float score;
        }

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public class Recapture {
            Boolean prediction;
            Float score;
        }

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public class AbnormalFace {
            Integer[] face_bbox;
            Boolean prediction;
            Float score;
        }

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public class AbnormalText {
            String type;
            Boolean prediction;
            Float score;
        }

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public class CornerCut {
            Boolean prediction;
            Float score;
        }

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public class IdFont {
            Boolean prediction;
            String date_of_issue;
            String id_font;
        }
    }

}
