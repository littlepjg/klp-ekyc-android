/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kalapa.antocr;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.google.android.material.snackbar.Snackbar;
import com.otaliastudios.cameraview.BitmapCallback;
import com.otaliastudios.cameraview.CameraUtils;
import com.kalapa.antocr.ui.camera.CameraSourcePreview;
import com.kalapa.antocr.ui.camera.GraphicOverlay;
import com.kalapa.antocr.utils.FaceUtils;
import com.kalapa.antocr.utils.FileUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static android.nfc.NfcAdapter.EXTRA_DATA;

/**
 * Activity for the face tracker app.  This app detects faces with the rear facing camera, and draws
 * overlay graphics to indicate the position, size, and ID of each face.
 */
public final class FaceTrackerActivity extends AppCompatActivity {
    private final String TAG = FaceTrackerActivity.class.getSimpleName();

    private CameraSource mCameraSource = null;

    private CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    private boolean mIsFrontFacing;
    public TextView mTvState;
    public int mState = 0;
    public boolean flagDelay = false;
    final Handler handler = new Handler();
    int count = 0;

    public ImageView mIvIcon;
    public ArrayList<Integer> number;
    //==============================================================================================
    // Activity Methods
    //==============================================================================================

    /**
     * Initializes the UI and initiates the creation of a face detector
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Log.i(TAG, "On Create");
        setContentView(R.layout.main);
        Intent intent = getIntent();
        mTvState = findViewById(R.id.tv_state);
        mIvIcon = findViewById(R.id.iv_icon);
        changeState(0);
        mIsFrontFacing = intent.getBooleanExtra("isFrontFacing", true);
        mPreview = findViewById(R.id.preview);
        mGraphicOverlay = findViewById(R.id.faceOverlay);

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource();
        } else {
            requestCameraPermission();
        }
        if (icicle != null) {
            mIsFrontFacing = icicle.getBoolean("IsFrontFacing");
        }
        number = new ArrayList<>();
        for (int i = 2; i <= 3; ++i) number.add(i);
        Log.d(TAG, number.toString());
        Collections.shuffle(number); // Suffle left right.

    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.ocr_permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ocr_ok, listener)
                .show();
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     */
    private void createCameraSource() {
        Context context = getApplicationContext();
        FaceDetector faceDetector = new FaceDetector.Builder(context)
                .setClassificationType(FaceDetector.ALL_LANDMARKS)
                .setProminentFaceOnly(true)
                .build();

        faceDetector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());

        if (!faceDetector.isOperational()) {
            // Note: The first time that an app using face API is installed on a device, GMS will
            // download a native library to the device in order to do detection.  Usually this
            // completes before the app is run for the first time.  But if that download has not yet
            // completed, then the above call will not detect any faces.
            //
            // isOperational() can be used to check if the required native library is currently
            // available.  The detector will automatically become operational once the library
            // download completes on device.
            Log.w(TAG, "Face detector dependencies are not yet available.");

        }
        int facing = CameraSource.CAMERA_FACING_FRONT;
        if (!mIsFrontFacing) {
            facing = CameraSource.CAMERA_FACING_BACK;
        }

        mCameraSource = new CameraSource.Builder(context, faceDetector)
                .setRequestedPreviewSize(1080, 720)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(30.0f)
                .build();

    }

    private void takePicture(final String fileName, final AntOcrCallback<Boolean> callback) {
        if (mCameraSource == null) return;
        try {
            mCameraSource.takePicture(null, new CameraSource.PictureCallback() {
                @Override
                public void onPictureTaken(@Nullable byte[] bytes) {
                    CameraUtils.decodeBitmap(bytes, 1080, 1080, new BitmapCallback() {
                        @Override
                        public void onBitmapReady(@Nullable Bitmap bitmap) {
                            FaceUtils faceUtils = new FaceUtils();
                            Log.d(TAG, bitmap.getWidth() + "/" + bitmap.getHeight());
                            Frame frame = new Frame.Builder().setBitmap(bitmap).build();

                            FaceDetector detector = new
                                    FaceDetector.Builder(FaceTrackerActivity.this)
                                    .setTrackingEnabled(false)
                                    .build();
                            SparseArray<Face> faces = detector.detect(frame);
                            Bitmap face = faceUtils.centerCrop(bitmap, faces);
                            FileUtils.saveFile(FaceTrackerActivity.this, face, fileName);
                            bitmap.recycle();
                            face.recycle();
                            detector.release();
                            callback.onOk(true);
                        }
                    });
                }
            });
        } catch (Exception e) {
            callback.onNG(e);
        }
    }

    private void openResult() {
//        Intent intent = new Intent(FaceTrackerActivity.this, CropResultActivity.class);
//        startActivity(intent);
        //finish();
        final Intent data = new Intent();
        data.putExtra(EXTRA_DATA, "OK");
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "On Restart");
        mState = 0;
        flagDelay = true;
        count = 0;
        changeState(mState);
        Collections.shuffle(number);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startCameraSource();
            }
        }, 1000);
        //startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        Log.i(TAG, "On Pause");
        super.onPause();
        mPreview.stop();
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "On Destroy");
        if (mPreview != null)
            mPreview.release();
        if (mCameraSource != null) {
            mCameraSource.release();
            mCameraSource = null;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "On Restart");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            createCameraSource();
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Face Tracker sample")
                .setMessage(R.string.ocr_no_camera_permission)
                .setPositiveButton(R.string.ocr_ok, listener)
                .show();
    }

    //==============================================================================================
    // Camera Source Preview
    //==============================================================================================

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
                delayRun(0);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    //==============================================================================================
    // Graphic Face Tracker
    //==============================================================================================

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay, mIsFrontFacing, FaceTrackerActivity.this);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);

        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }

    /**
     * Saves the camera facing mode, so that it can be restored after the device is rotated.
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean("IsFrontFacing", mIsFrontFacing);
    }

    public void changeState(int i) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                flagDelay = false;
            }
        }, 3000);

        switch (i) {
            case 0:
                mTvState.setText(getString(R.string.face_detect));
                mIvIcon.setImageResource(R.drawable.liveness_head);
                break;
            case 1:
                mTvState.setText(getString(R.string.close_eye));
                mIvIcon.setImageResource(R.drawable.liveness_eye);
                break;
            case 2:
                mTvState.setText(getString(R.string.rotate_right));
                mIvIcon.setImageResource(R.drawable.liveness_head_right);
                break;
            case 3:
                mTvState.setText(getString(R.string.rotate_left));
                mIvIcon.setImageResource(R.drawable.liveness_head_left);
                break;

            case 20:
                mTvState.setText(getString(R.string.detect_success));
                mIvIcon.setImageResource(R.drawable.result_success);
                count = 0;
                mState = 0;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //takePicture(FileUtils.faceFileName);
                        openResult();
                    }
                }, 500);

                // mState = 0;
                break;

            default: {

            }
        }
    }

    public void showStateSuccess(int i) {
        mTvState.setText(getString(R.string.detect_success));
    }

    public void delayRun(int i) {
        Log.d("check delay", "check delay:" + flagDelay);
        if (flagDelay) {
            return;
        }
        flagDelay = true;
        Log.d("check delay", "check status:" + count + "--" + i);
        AntOcrCallback<Boolean> callback = new AntOcrCallback<Boolean>() {
            @Override
            public void onOk(Boolean data) {
                if (count < number.size()) {
                    mState = number.get(count);
                    count++;
                } else {
                    mState = 20;
                }
                showStateSuccess(mState);
                // mState = state;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 1000ms
                        changeState(mState);
                    }
                }, 1000);
            }

            @Override
            public void onNG(Exception e) {
                changeState(mState);
                // Failed?
            }
        };
        if (mState == 0) {
            takePicture(FileUtils.faceFileName, callback);
        } else if (mState == 1) {//close eye
            takePicture(FileUtils.faceFileNameCloseEyes, callback);
        } else if (mState == 2) { //right
            takePicture(FileUtils.faceFileNameRight, callback);
        } else if (mState == 3) { //left
            takePicture(FileUtils.faceFileNameLeft, callback);
        } else {
            callback.onOk(true);
        }

    }
}
