package com.kalapa.antocr;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kalapa.antocr.databinding.ActivityCropResultBinding;
import com.kalapa.antocr.model.CardInfo;
import com.kalapa.antocr.model.FaceVerifyInfo;
import com.kalapa.antocr.model.FaceVerifyRequest;
import com.kalapa.antocr.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class CropResultActivity extends AppCompatActivity {
    Bitmap faceBitmap;
    Bitmap cardBitmap;
    Bitmap cardBitmapBack;

    ActivityCropResultBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCropResultBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initLayout();

        final File cardFile = FileUtils.getCardFile(this);
        File faceFile = FileUtils.getFaceFile(this);
        File cardBackFile = FileUtils.getCardBackFile(this);
        RequestOptions requestOptions = new RequestOptions();
        //int round = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        //requestOptions = requestOptions.transform(new RoundedCornersTransformation(8, 0, RoundedCornersTransformation.CornerType.BOTTOM));


        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) binding.cardImage.getLayoutParams();
        params.height = getResources().getDisplayMetrics().heightPixels / 3;

        if (cardFile.exists()) {
            binding.cardImage.setLayoutParams(params);
            cardBitmap = BitmapFactory.decodeFile(cardFile.getAbsolutePath());
            Glide.with(this)
                    .load(cardBitmap)
                    .apply(requestOptions)
                    .into(binding.cardImage);

            AntOcr.getFrontCard(cardFile, new AntOcrCallback<CardInfo>() {
                @Override
                public void onOk(CardInfo data) {
                    setCardVerifyInfo(data);
                    binding.loadingCard.setVisibility(View.GONE);
                    binding.loadingAnomaly.setVisibility(View.VISIBLE);
                }

                @Override
                public void onNG(Exception e) {
                    Log.d(CropResultActivity.class.getName(), e.getMessage());
                    binding.loadingCard.setVisibility(View.GONE);
                    Toast.makeText(CropResultActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

        if (cardBackFile.exists()) {
            binding.llCardBack.setVisibility(View.VISIBLE);
            binding.cardImageBack.setLayoutParams(params);
            cardBitmapBack = BitmapFactory.decodeFile(cardBackFile.getAbsolutePath());
            Glide.with(this)
                    .load(cardBitmapBack)
                    .apply(requestOptions)
                    .into(binding.cardImageBack);

            AntOcr.getBackCard(cardBackFile, new AntOcrCallback<CardInfo>() {
                @Override
                public void onOk(CardInfo data) {
                    setCardBackVerifyInfo(data);
                    binding.loadingCard.setVisibility(View.GONE);
                }

                @Override
                public void onNG(Exception e) {
                    e.printStackTrace();
                    Log.d(CropResultActivity.class.getName(), e.getMessage());
                    Toast.makeText(CropResultActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            binding.llCardBack.setVisibility(View.INVISIBLE);
        }

        faceBitmap = BitmapFactory.decodeFile(faceFile.getAbsolutePath());
        Glide.with(this)
                .load(faceBitmap)
                .apply(RequestOptions.circleCropTransform())
                .into(binding.faceImage);

        binding.loadingCard.setVisibility(View.VISIBLE);
        binding.loadingFace.setVisibility(View.VISIBLE);

        if (AntOcr.isCheckRealFace()) {
            FaceVerifyRequest faceVerifyRequest = new FaceVerifyRequest();
            faceVerifyRequest.setCard(FileUtils.getCardFile(this));
            faceVerifyRequest.setCheck3RandomPose(true);
            List<File> facesFile = new ArrayList<>();
            facesFile.add(FileUtils.getFaceFile(this));
            facesFile.add(FileUtils.getFaceFileLeft(this));
            facesFile.add(FileUtils.getFaceFileRight(this));
            facesFile.add(FileUtils.getFaceFile(this));
            faceVerifyRequest.setFaces(facesFile);
            AntOcr.faceVerify(faceVerifyRequest.getFaces().get(0), new AntOcrCallback<FaceVerifyInfo>() {
                @Override
                public void onOk(FaceVerifyInfo data) {
                    binding.loadingFace.setVisibility(View.GONE);
                    setFaceVerifyInfo(data);
                }

                @Override
                public void onNG(Exception e) {
                    binding.loadingFace.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(CropResultActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            AntOcr.faceVerify(faceFile, new AntOcrCallback<FaceVerifyInfo>() {
                @Override
                public void onOk(FaceVerifyInfo data) {
                    binding.loadingFace.setVisibility(View.GONE);
                    setFaceVerifyInfo(data);
                }

                @Override
                public void onNG(Exception e) {
                    binding.loadingFace.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(CropResultActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

        binding.btFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(CropResultActivity.this, CardScanningActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
                finish();
            }
        });
        final View registerText = binding.tvRegister;
        final View loadingRegister = binding.loadingRegister;
        binding.tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CropResultActivity.this, "This features is developing", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initLayout() {

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setCardVerifyInfo(CardInfo cardInfo) {
        if (cardInfo == null) return;

        setTextInfo(binding.tvFullName, cardInfo.getData().getFields().getName());
        setTextInfo(binding.cardNumber, cardInfo.getData().getFields().getId_number());
        setTextInfo(binding.birthday, cardInfo.getData().getFields().getBirthday());

        setTextInfo(binding.sex, cardInfo.getData().getFields().getGender());
        setTextInfo(binding.address, cardInfo.getData().getFields().getResident());
        setTextInfo(binding.expireDate, cardInfo.getData().getFields().getDoe());

//        setTextInfo(binding.national, cardInfo.getData().getFields().getNational());
        setTextInfo(binding.country, cardInfo.getData().getFields().getReligion());

    }

    public void setCardBackVerifyInfo(CardInfo cardInfo) {
        if (cardInfo == null) return;

        setTextInfo(binding.ethnicity, cardInfo.getData().getFields().getEthnicity());
        setTextInfo(binding.religion, cardInfo.getData().getFields().getReligion());
        setTextInfo(binding.issueDate, cardInfo.getData().getFields().getDoi());
        setTextInfo(binding.issueBy, cardInfo.getData().getFields().getPoi());

    }

    public void setTextInfo(TextView textView, String info) {
        if (info == null || info.isEmpty()) {
            ((View) textView.getParent()).setVisibility(View.GONE);
        } else {
            ((View) textView.getParent()).setVisibility(View.VISIBLE);
            textView.setText(info);
        }

    }

    public void setFaceVerifyInfo(FaceVerifyInfo cardInfo) {
        if (cardInfo == null) return;
        binding.sim.setText(String.valueOf(cardInfo.getData().getMatching_score()));
        String verifyText;
        int verifyTextColor;
        if (cardInfo.getData().getIs_matched() != null) {
            if (cardInfo.getData().getIs_matched()) {
                verifyText = getString(R.string.ocr_sim);
                verifyTextColor = Color.rgb(0, 255, 0);
            } else {
                verifyText = getString(R.string.ocr_not_sim);
                verifyTextColor = Color.rgb(255, 0, 0);
            }
        } else {
            verifyText = getString(R.string.ocr_maybe_same);
            verifyTextColor = Color.rgb(255, 165, 0);
        }
        TextView textViewResult = binding.verifyResult;
        textViewResult.setText(verifyText);
        textViewResult.setTextColor(verifyTextColor);

        // Pretend that face is always true
        binding.faceAntiSpoofStatus.setTextColor(Color.rgb(0, 255, 0));
        binding.faceAntiSpoofStatus.setText("REAL");

        // TODO: Check Face is Fraud
//        if (cardInfo.getFaceAntiSpoofStatus() != null) {
//            FaceAntiSpoofStatus faceStatus = cardInfo.getFaceAntiSpoofStatus();
//            binding.faceAntiSpoofStatus.setText(faceStatus.getStatus());
//            if (faceStatus.getStatus() != null && faceStatus.getStatus().toLowerCase().contains("fake")) {
//                binding.faceAntiSpoofStatus.setTextColor(Color.rgb(255, 0, 0));
//            } else {
//                binding.faceAntiSpoofStatus.setTextColor(Color.rgb(0, 255, 0));
//            }
//        }
        // Return face. Ignored
//        long[] locations = cardInfo.getFaceLOC();
//        if (locations != null && locations.length >= 4 && cardBitmap != null && !cardBitmap.isRecycled() &&
//                cardBitmap.getWidth() > 0 &&
//                cardBitmap.getHeight() > 0) {
//            int x = (int) locations[0];
//            int y = (int) locations[1];
//            int width = (int) locations[2];
//            int height = (int) locations[3];
//
//            if (x + width >= cardBitmap.getWidth()) {
//                width = cardBitmap.getWidth() - x;
//            }
//            if (y + height >= cardBitmap.getHeight()) {
//                height = cardBitmap.getHeight() - y;
//            }
//            if (width > 0 && height > 0) {
//                Log.d(CropResultActivity.this.getClass().getSimpleName(), "width: " + width + " height: " + height);

//                Bitmap faceCard = Bitmap.createBitmap(cardBitmap, x, y, width, height);
//                Glide.with(CropResultActivity.this)
//                        .load(faceCard)
//                        .into(faceCardImage);
//            }
//        }

    }
}

