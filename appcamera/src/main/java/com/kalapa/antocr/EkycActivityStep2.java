package com.kalapa.antocr;

import static com.kalapa.antocr.SessionManager.checkIfSessionExpired;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.LIVENESS_URI;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.kalapa.antocr.databinding.ActivityEkycStep2Binding;
import com.kalapa.antocr.utils.FileUtils;
import com.kalapa.antocr.utils.SDKUtils;


public class EkycActivityStep2 extends AppCompatActivity {
    ActivityEkycStep2Binding binding;
    private static final String TAG = EkycActivityStep2.class.getName();
    String sessionObject;
    String selfieUrl;

    String livenessMessage = "";
    int intentResultCode = RESULT_CANCELED;
    int livenessResultCode = SDKUtils.LIVENESS_CODE.UNVERIFIED;
    private static final int LIVENESS_CODE = 101;
    private long lastClick = System.currentTimeMillis();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEkycStep2Binding.inflate(getLayoutInflater());
        if (getSupportActionBar() != null) getSupportActionBar().hide();
        setContentView(binding.getRoot());
        onPreExecute();
        onUIUpdate();
    }

    private boolean isClickable() {
        if (System.currentTimeMillis() - lastClick > 1000) {
            lastClick = System.currentTimeMillis();
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LIVENESS_CODE) {
            if (data != null) {
                livenessMessage = data.getStringExtra(SDKUtils.SDK_CONFIG.LIVENESS_RESULT);
                livenessResultCode = data.getIntExtra(SDKUtils.SDK_CONFIG.LIVENESS_RESULT_CODE, RESULT_CANCELED);
                selfieUrl = data.getStringExtra(SDKUtils.SDK_CONFIG.LIVENESS_URI);
                intentResultCode = resultCode;
                binding.ivSelfie.setImageBitmap(BitmapFactory.decodeFile(selfieUrl));
                binding.tvSelfieResult.setText(livenessMessage);
                binding.tvSelfieResult.setTextColor(resultCode == RESULT_OK ? getResources().getColor(R.color.ekyc_green) : getResources().getColor(R.color.ekyc_red));
            }
            onUIUpdate();
            shouldContinue();
        }

    }

    private void onExecute() {
        AntOcr.init(sessionObject, SessionManager.getHost());
        binding.btnContinue.setOnClickListener((v) -> {
            shouldContinue();
        });

        binding.btnTakeSelfie.setOnClickListener((v) -> {
            if (isClickable()) {
                Intent intent = new Intent(EkycActivityStep2.this, LivenessActivity.class);
                intent.putExtra(SDKUtils.SDK_CONFIG.SESSION_ID, sessionObject);
                startActivityForResult(intent, LIVENESS_CODE);
            }
        });
    }

    private void shouldContinue() {
        if (selfieUrl != null && intentResultCode == RESULT_OK) {
            preDestroy();
            // Trả kết quả về cho EKYC để gọi bảng kết quả Final.
        } else {
            Toast.makeText(EkycActivityStep2.this, "Vui lòng chụp ảnh hợp lệ trước khi tiếp tục", Toast.LENGTH_LONG).show();
        }
    }

    private void onUIUpdate() {
//        if (selfieUrl == null || livenessResultCode != SDKUtils.LIVENESS_CODE.VERIFIED) {
//            binding.btnContinue.setClickable(false);
//            binding.btnContinue.setBackgroundColor(getResources().getColor(R.color.ekyc_grey));
//        } else {
//            binding.btnContinue.setBackgroundColor(getResources().getColor(R.color.ekyc_btn_color));
//            binding.btnContinue.setClickable(true);
//        }
    }


    private void onPreExecute() {
        Intent intent = getIntent();
        String token = intent.getStringExtra(SDKUtils.SDK_CONFIG.TOKEN);
        String session = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_ID);
        if (session != null) {
            if (checkIfSessionExpired(EkycActivityStep2.this, session)) {
                Log.d(TAG, "Old Session is still  Ok. " + session);
                sessionObject = session;
                onExecute();
            } else {
                // Session is expired
                livenessMessage = SDKUtils.ERROR_CODE.SESSION_EXPIRED;
                livenessResultCode = SDKUtils.OCR_CODE.EXPIRED;
                intentResultCode = RESULT_CANCELED;
                preDestroy();
            }
        } else {
            livenessMessage = SDKUtils.ERROR_CODE.NETWORK_ERROR;
            livenessResultCode = SDKUtils.OCR_CODE.OTHER_ERROR;
            intentResultCode = RESULT_CANCELED;
            preDestroy();
        }
    }


    private void preDestroy() {
        if (isClickable()) {
            Intent intent = getIntent();
            intent.putExtra(SDKUtils.SDK_CONFIG.LIVENESS_RESULT_CODE, livenessResultCode);
            intent.putExtra(SDKUtils.SDK_CONFIG.LIVENESS_RESULT, livenessMessage);
            intent.putExtra(LIVENESS_URI, selfieUrl);
            setResult(intentResultCode, intent);
            finish();
        }
    }
}
