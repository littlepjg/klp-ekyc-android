package com.kalapa.antocr.components;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kalapa.antocr.R;
import com.kalapa.antocr.model.ItemView;

import java.util.ArrayList;

public class ItemListViewAdapter extends BaseAdapter {
    final ArrayList<ItemView> listItem;

    public ItemListViewAdapter(ArrayList<ItemView> itemViews) {
        this.listItem = itemViews;
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int i) {
        return listItem.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View viewItem;
        if (view == null) {
            viewItem = View.inflate(viewGroup.getContext(), R.layout.item_adapter, null);
        } else viewItem = view;
        ItemView itemView = (ItemView) getItem(i);
        ((TextView) viewItem.findViewById(R.id.tv_label)).setText(itemView.getLabel());
        ((TextView) viewItem.findViewById(R.id.tv_value)).setText(itemView.getValue());
        return viewItem;
    }
}
