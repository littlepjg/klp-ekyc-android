package com.kalapa.antocr.components;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.kalapa.antocr.R;
import com.kalapa.antocr.model.ItemView;

import java.util.ArrayList;

public class AnomalyListViewAdapter extends BaseAdapter {
    final ArrayList<ItemView> listItem;

    public AnomalyListViewAdapter(ArrayList<ItemView> itemViews) {
        this.listItem = itemViews;
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public Object getItem(int i) {
        return listItem.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View viewItem;
        if (view == null) {
            viewItem = View.inflate(viewGroup.getContext(), R.layout.anomaly_adapter, null);
        } else viewItem = view;
        ItemView itemView = (ItemView) getItem(i);
        ((TextView) viewItem.findViewById(R.id.tv_label)).setText(itemView.getLabel());
        ((TextView) viewItem.findViewById(R.id.tv_value)).setTextColor(itemView.getIsGood() != null ? itemView.getIsGood() ? ContextCompat.getColor(viewGroup.getContext(),R.color.ekyc_red) :
                ContextCompat.getColor(viewGroup.getContext(),R.color.ekyc_green) :  ContextCompat.getColor(viewGroup.getContext(),R.color.ocr_colorAccent));
        ((TextView) viewItem.findViewById(R.id.tv_value)).setText(itemView.getValue());
        ((ImageView) viewItem.findViewById(R.id.iv_value)).setImageResource(itemView.getIsGood() != null ? itemView.getIsGood() ? R.drawable.not_ok :
                R.drawable.ok : R.drawable.ic_minus_98);

        return viewItem;
    }
}
