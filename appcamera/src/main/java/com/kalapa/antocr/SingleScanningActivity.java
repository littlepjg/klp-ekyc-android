/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kalapa.antocr;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kalapa.antocr.utils.BitmapUtil;
import com.kalapa.antocr.utils.ImageUtils;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.kalapa.antocr.model.BaseObserver;
import com.kalapa.antocr.model.CapturedPicture;
import com.kalapa.antocr.model.PictureType;
import com.kalapa.antocr.utils.FileUtils;
import com.otaliastudios.cameraview.PictureResult;
import com.otaliastudios.cameraview.gesture.Gesture;
import com.otaliastudios.cameraview.gesture.GestureAction;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class SingleScanningActivity extends AppCompatActivity {
    private PublishSubject<byte[]> _cropTask = PublishSubject.create();

    int widthScreen;
    int heightScreen;
    private boolean mCapturingPicture;
    CameraView camera;
    View frame;
    ImageView takePicture;
    View loadingview;
    View imageCapturedHolder;
    Button btConfirm;
    TextView tvTitle;
    Button btRetake;
    ImageView imageCaptured;
    private String tag = SingleScanningActivity.class.getSimpleName();
    PictureType runMode;
    boolean isFront;
    RequestOptions requestOptions;
    ImageView icGuide;
    Button btnCancel;
    TextView txtGuide;
    private boolean showOnce = false;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        showOnce = false;
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        setContentView(R.layout.card_scanning_activity);
        runMode = (PictureType) getIntent().getSerializableExtra("type");
        Log.d(tag, "" + runMode);
        isFront = runMode == PictureType.CARD_FRONT;
        initLayout();

        if (isFront) {
            frame.setBackgroundDrawable(ContextCompat.getDrawable(SingleScanningActivity.this, R.drawable.identity_front_frame));
            icGuide.setImageResource(R.drawable.step_back_not_ready);
        } else {
            frame.setBackgroundDrawable(ContextCompat.getDrawable(SingleScanningActivity.this, R.drawable.identity_back_frame));
            icGuide.setImageResource(R.drawable.step_back_capture);
        }
        camera.setLifecycleOwner(this);
        camera.mapGesture(Gesture.TAP, GestureAction.AUTO_FOCUS); // Tap to focus!

        camera.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(@NonNull PictureResult result) {
                super.onPictureTaken(result);
                Log.d(tag, "onPictureTaken");
                _cropTask.onNext(result.getData());
            }
        });
        handlerImageCaptured();

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCapturingPicture) return;
                mCapturingPicture = true;
                camera.takePicture();
                loadingview.setVisibility(View.VISIBLE);
                txtGuide.setText("Xin kiểm tra lại ảnh đã chụp và xác nhận hoặc chụp lại.");
            }
        });
        imageCapturedHolder.setVisibility(View.GONE);

        btnCancel.setOnClickListener((v) -> {
            onBackPressed();
        });

        btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageCapturedHolder.setVisibility(View.GONE);
                takePicture.setVisibility(View.VISIBLE);
                finish();
                genResult();
            }
        });

        btRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageCapturedHolder.setVisibility(View.GONE);
                takePicture.setVisibility(View.VISIBLE);
                txtGuide.setText("Vui lòng đưa giấy tờ hợp lệ vào trong khung, chạm vào màn hình để lấy nét và chụp.");
            }
        });
        requestOptions = new RequestOptions();
        requestOptions = requestOptions.transform(new CenterCrop(), new RoundedCorners(1));
    }

    private void initLayout() {
        camera = findViewById(R.id.camera);
        frame = findViewById(R.id.frame);
        takePicture = findViewById(R.id.takePicture);
        loadingview = findViewById(R.id.loadingview);
        imageCapturedHolder = findViewById(R.id.imageCapturedHolder);
        btConfirm = findViewById(R.id.bt_confirm);
//        tvTitle = findViewById(R.id.tv_title);
        btRetake = findViewById(R.id.bt_retake);
        imageCaptured = findViewById(R.id.imageCaptured);
        icGuide = findViewById(R.id.ic_guide);
        btnCancel = findViewById(R.id.btn_cancel);
        txtGuide = findViewById(R.id.txt_guide);
//        if (isFront) {
//            tvTitle.setText(R.string.ocr_confirm_photo_mess_front);
//        } else {
//            tvTitle.setText(R.string.ocr_confirm_photo_mess_back);
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                if (AntOcr.pictureCallback != null) {
                    genResult();
                }
            }
            finish();
        }
    }

    public void startFaceActivity() {
        if (AntOcr.isCheckRealFace()) {
            Intent intent = new Intent(SingleScanningActivity.this, FaceTrackerActivity.class);
            startActivityForResult(intent, 100);
        } else {
            Intent intent = new Intent(SingleScanningActivity.this, FaceCaptureActivity.class);
            startActivityForResult(intent, 100);
        }
    }

    private void genResult() {
        ArrayList<CapturedPicture> images = new ArrayList<>();
        if (runMode == PictureType.CARD_FRONT) {
            images.add(new CapturedPicture(
                    FileUtils.getCardFile(SingleScanningActivity.this),
                    PictureType.CARD_FRONT
            ));
        }
        if (runMode == PictureType.CARD_BACK) {
            images.add(new CapturedPicture(
                    FileUtils.getCardBackFile(this),
                    PictureType.CARD_BACK
            ));
        } else {
            images.add(new CapturedPicture(
                    FileUtils.getFaceFile(this),
                    PictureType.FACE
            ));
            images.add(new CapturedPicture(
                    FileUtils.getFaceFileCloseEyes(this),
                    PictureType.FACE
            ));
            images.add(new CapturedPicture(
                    FileUtils.getFaceFileRight(this),
                    PictureType.FACE
            ));

            images.add(new CapturedPicture(
                    FileUtils.getFaceFileLeft(this),
                    PictureType.FACE
            ));
        }
        if (AntOcr.pictureCallback != null) {
            AntOcr.pictureCallback.onOk(images);
        }
    }

    private void handlerImageCaptured() {
        _cropTask.subscribeOn(Schedulers.computation())
                .map(new Function<byte[], Bitmap>() {
                    @Override
                    public Bitmap apply(byte[] capturedImage) {
//                        Bitmap bitmap = CameraUtils.decodeBitmap(capturedImage, 1080, 1080, new BitmapFactory.Options());
                        Bitmap bitmap = CameraUtils.decodeBitmap(capturedImage);
                        try {
                            bitmap = BitmapUtil.centerCrop(bitmap,
                                    ImageUtils.getScreenDisplayMetrics(SingleScanningActivity.this).widthPixels,
                                    ImageUtils.getScreenDisplayMetrics(SingleScanningActivity.this).widthPixels * 3 / 4
                            );
                            if (isFront) {
                                FileUtils.saveFile(SingleScanningActivity.this, bitmap, FileUtils.cardFileName);
                            } else {
                                FileUtils.saveFile(SingleScanningActivity.this, bitmap, FileUtils.cardBackFileName);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (isFront) {
                                FileUtils.saveFile(SingleScanningActivity.this, bitmap, FileUtils.cardFileName);
                            } else {
                                FileUtils.saveFile(SingleScanningActivity.this, bitmap, FileUtils.cardBackFileName);
                            }
                        }
                        return bitmap;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<Bitmap>() {
                    @Override
                    public void onNext(Bitmap bitmap) {
                        super.onNext(bitmap);
                        loadingview.setVisibility(View.GONE);
                        imageCapturedHolder.setVisibility(View.VISIBLE);
                        takePicture.setVisibility(View.GONE);

                        Glide.with(SingleScanningActivity.this).load(bitmap).apply(requestOptions)
                                .into(imageCaptured);
                        mCapturingPicture = false;
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mCapturingPicture = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mCapturingPicture = false;
                    }
                });
    }


    @Override
    protected void onResume() {
        Log.d(tag, "onResume");
        super.onResume();
        boolean permissionCamera = checkPermissionCamera();
        if (runMode == PictureType.FACE && permissionCamera) {
            startFaceActivity();
        }
    }


    @Override
    protected void onPause() {
        Log.d(tag, "on Pause");
        if (camera != null)
            camera.stopVideo();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d(tag, "on Destroy");
        super.onDestroy();
        if (camera != null)
            camera.destroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(tag, "onRequestPermissionsResult");
    }

    private void dismissSettingsPermission() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

    private void showSettingsPermission() {
        if (!showOnce) {
            showOnce = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Thông báo");
            builder.setMessage("Ứng dụng chưa được cấp quyền sử dụng camera, nhấn vào cài đặt để tiếp tục");
            builder.setCancelable(false);
            builder.setPositiveButton("Cài đặt", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                    dialogInterface.dismiss();
                }
            });
            builder.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    int permissionGranted = ContextCompat.checkSelfPermission(SingleScanningActivity.this, Manifest.permission.CAMERA);
                    if (permissionGranted != PackageManager.PERMISSION_GRANTED) {
                        finish();
                    }
                }
            });
            alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private boolean checkPermissionCamera() {
        int permissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionGranted != PackageManager.PERMISSION_GRANTED) {
            Log.d(tag, "Permission Not Granted");
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 222);
            } else {
                if (permissionGranted == PackageManager.PERMISSION_DENIED)
                    showSettingsPermission();
            }
            return false;
        } else {
            Log.d(tag, "Permission Granted");
            dismissSettingsPermission();
            camera.setLifecycleOwner(this);
            return true;
        }
    }

}
