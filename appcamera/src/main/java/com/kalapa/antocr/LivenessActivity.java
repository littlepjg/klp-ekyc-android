package com.kalapa.antocr;

import static com.kalapa.antocr.SessionManager.checkIfSessionExpired;
import static com.kalapa.antocr.utils.FileUtils.faceFileName;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.LIVENESS_RESULT;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.LIVENESS_RESULT_CODE;
import static com.kalapa.antocr.utils.SDKUtils.SDK_CONFIG.LIVENESS_URI;
import static org.webrtc.SessionDescription.Type.ANSWER;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.kalapa.antocr.databinding.ActivitySamplePeerConnectionBinding;
import com.kalapa.antocr.handlers.SendOfferHttpHandler;
import com.kalapa.antocr.model.OfferResponse;
import com.kalapa.antocr.model.SessionObject;
import com.kalapa.antocr.model.SimpleSdpObserver;
import com.kalapa.antocr.utils.FileUtils;
import com.kalapa.antocr.utils.SDKUtils;
import com.kalapa.antocr.utils.WebRTCUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.DataChannel;
import org.webrtc.DefaultVideoDecoderFactory;
import org.webrtc.DefaultVideoEncoderFactory;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.RtpReceiver;
import org.webrtc.RtpSender;
import org.webrtc.RtpTransceiver;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Objects;

public class LivenessActivity extends AppCompatActivity {
    String livenessMessage = "";
    int intentResultCode = RESULT_CANCELED;
    int livenessResultCode = SDKUtils.LIVENESS_CODE.UNVERIFIED;
    boolean isTimedOut = false;
    private static final int CAMERA_REQUEST = 188;
    PeerConnection peerConnection;
    PeerConnectionFactory factory;
    DataChannel dataChannel;
    ActivitySamplePeerConnectionBinding binding;
    private EglBase rootEglBase;
    private static final String TAG = "LivenessActivity";
    private static final int RC_CALL = 111;
    public static final String VIDEO_TRACK_ID = "ANDROID" + System.currentTimeMillis();
    public static final int VIDEO_RESOLUTION_WIDTH = 720;
    public static final int VIDEO_RESOLUTION_HEIGHT = 1080;
    public static final int FPS = 30;
    private VideoTrack videoTrackFromCamera;
    private MediaStream localStream;
    private long lastClick = System.currentTimeMillis();

    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(this);
    }

    String sessionObject;
    VideoCapturer videoCapturer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySamplePeerConnectionBinding.inflate(getLayoutInflater());
        if (getSupportActionBar() != null) getSupportActionBar().hide();
        setContentView(binding.getRoot());
        onPreExecute();
    }

    private void onExecute() {
        if (ContextCompat.checkSelfPermission(LivenessActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LivenessActivity.this, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
        } else {
            start();
        }
    }

    private void onPreExecute() {
        Intent intent = getIntent();
        String token = intent.getStringExtra(SDKUtils.SDK_CONFIG.TOKEN);
        String session = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_ID);
        Toast.makeText(LivenessActivity.this, "Hệ thống đang khởi tạo, vui lòng đợi giây lát", Toast.LENGTH_LONG).show();
        String DEFAULT_PLAN = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_PLAN);
        Boolean SHOW_ALL_RESULT = intent.getBooleanExtra(SDKUtils.SDK_CONFIG.SESSION_SHOW_ALL_RESULT, false);
        String CALLBACK_URL = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_CALLBACK_URL);
        String UPLOAD_TYPE = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_UPLOAD_TYPE);

        if (session != null) {
            if (checkIfSessionExpired(LivenessActivity.this, session)) {
                Log.d(TAG, "Old Session is still  Ok. " + session);
                sessionObject = session;
                onExecute();
            } else {
                // Session is expired
                livenessMessage = SDKUtils.ERROR_CODE.SESSION_EXPIRED;
                livenessResultCode = SDKUtils.OCR_CODE.EXPIRED;
                intentResultCode = RESULT_CANCELED;
                onPreDestroy();
            }
        } else {
            new SessionManager(token).getSessionToken(this,DEFAULT_PLAN, SHOW_ALL_RESULT, UPLOAD_TYPE, CALLBACK_URL, new AntOcrCallback<SessionObject>() {
                @Override

                public void onOk(SessionObject data) {
                    Log.d(TAG, "Get new Session Ok. " + data.getToken());
                    sessionObject = data.getToken();
                    onExecute();
                }

                @Override
                public void onNG(Exception e) {
                    // Get Token Problem
                    e.printStackTrace();
                    livenessMessage = SDKUtils.ERROR_CODE.NETWORK_ERROR;
                    livenessResultCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResultCode = RESULT_CANCELED;
                    onPreDestroy();
                }
            });
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_REQUEST) {
            if (ContextCompat.checkSelfPermission(LivenessActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                start();
            } else {
                Toast.makeText(LivenessActivity.this, "Ứng dụng cần quyền truy cập Camera để có thể hoạt động", Toast.LENGTH_LONG).show();
                livenessMessage = SDKUtils.ERROR_CODE.PERMISION_NOT_ALLOWED;
                livenessResultCode = SDKUtils.LIVENESS_CODE.UNVERIFIED;
                intentResultCode = RESULT_CANCELED;
                finish();
            }

        }
    }

    private void onPreDestroy() {
        if (isClickable()) {
            stop();
            Intent intent = getIntent();
            intent.putExtra(LIVENESS_RESULT, livenessMessage);
            intent.putExtra(LIVENESS_RESULT_CODE, livenessResultCode);
            if (livenessResultCode == SDKUtils.LIVENESS_CODE.VERIFIED)
                intent.putExtra(LIVENESS_URI, FileUtils.getFaceFile(LivenessActivity.this).getAbsolutePath());
            setResult(intentResultCode, intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if (videoCapturer != null && videoCapturer.isScreencast()) {
                Log.d(TAG, "Video Capturer still Screencasting");
                videoCapturer.stopCapture();
            }
            videoCapturer.dispose();
            videoTrackFromCamera.removeSink(binding.surfaceView);
            videoTrackFromCamera.dispose();
            videoTrackFromCamera = null;
            videoCapturer = null;
            factory.dispose();
            factory = null;
        } catch (Exception e) {
            Log.e(TAG, "InterupException: " + e.getMessage());
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void stop() {
        try {
            if (dataChannel != null)
                dataChannel.close();
            if (peerConnection.getTransceivers() != null) {
                for (RtpTransceiver transceiver : peerConnection.getTransceivers()) {
                    transceiver.stop();
                    transceiver.dispose();
                }
            }

            if (peerConnection.getSenders() != null) {
                for (RtpSender sender : peerConnection.getSenders()) {
                    Objects.requireNonNull(sender.track()).dispose();
                    sender.dispose();
                    peerConnection.removeTrack(sender);
                }
            }

            binding.surfaceView.release();
            binding.surfaceView.clearImage();
//            binding.frCamera.setVisibility(View.GONE);
            binding.surfaceView.setVisibility(View.VISIBLE);
            binding.ivSelfieResult.setVisibility(View.VISIBLE);
            binding.btnHolder.setVisibility(View.VISIBLE);
            peerConnection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isClickable() {
        if (System.currentTimeMillis() - lastClick > 500) {
            lastClick = System.currentTimeMillis();
            return true;
        }
        return false;
    }

    private void initializeSurfaceViews() {
        rootEglBase = EglBase.create();
        binding.surfaceView.init(rootEglBase.getEglBaseContext(), null);
        binding.surfaceView.setEnableHardwareScaler(true);
        binding.surfaceView.setMirror(true);
        binding.surfaceView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);

        binding.btnConfirm.setOnClickListener((v) -> {
            livenessMessage = SDKUtils.ERROR_CODE.SUCCEED;
            livenessResultCode = SDKUtils.LIVENESS_CODE.VERIFIED;
            intentResultCode = RESULT_OK;
            onPreDestroy();
        });

        binding.btnCancel.setOnClickListener((v) -> {
            livenessMessage = SDKUtils.ERROR_CODE.CANCELED;
            livenessResultCode = SDKUtils.LIVENESS_CODE.UNVERIFIED;
            intentResultCode = RESULT_CANCELED;
            onPreDestroy();
        });

        binding.btnRetry.setOnClickListener((v) -> {
            livenessResultCode = SDKUtils.LIVENESS_CODE.UNVERIFIED;
            binding.tvDataMessage.setText(SDKUtils.getLivenessMessage(livenessResultCode, "Initializing"));
            binding.ivGuideIcon.setImageResource(SDKUtils.setIconGuide("Initializing"));
            stop();
            start();
        });
    }

    private void startNegotiate(@NonNull SessionDescription description) {
        SessionDescription.Type type = description.type;
        try {
            Log.d(TAG, sessionObject);
            String sdp = WebRTCUtils.sdpFilterCodec("video", "VP8/90000", description);
            Log.d(TAG, "type.canonicalForm()" + type.canonicalForm());
//            String sdp = description.description;
            OfferResponse response = new SendOfferHttpHandler(LivenessActivity.this).execute(sessionObject, type.canonicalForm(), description.description).get();
            peerConnection.setRemoteDescription(new SimpleSdpObserver(), new SessionDescription(ANSWER, response.getSdp()));
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Error:" + e.getMessage());
            livenessMessage = SDKUtils.ERROR_CODE.NETWORK_ERROR + " " + e.getMessage();
            livenessResultCode = SDKUtils.LIVENESS_CODE.UNVERIFIED;
            intentResultCode = RESULT_CANCELED;
            onPreDestroy();
        }
    }

    private void initializePeerConnectionFactory() {
        PeerConnectionFactory.InitializationOptions initializationOptions = PeerConnectionFactory.InitializationOptions.builder(this)
                .setEnableInternalTracer(true)
                .createInitializationOptions();
        PeerConnectionFactory.initialize(initializationOptions);

        PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();

        DefaultVideoEncoderFactory defaultVideoEncoderFactory = new DefaultVideoEncoderFactory(rootEglBase.getEglBaseContext(), true, true);
        DefaultVideoDecoderFactory defaultVideoDecoderFactory = new DefaultVideoDecoderFactory(rootEglBase.getEglBaseContext());
        factory = PeerConnectionFactory.builder()
                .setOptions(options)
                .setVideoEncoderFactory(defaultVideoEncoderFactory)
                .setVideoDecoderFactory(defaultVideoDecoderFactory)
                .createPeerConnectionFactory();
    }

    private void initializePeerConnections() {
        ArrayList<PeerConnection.IceServer> iceServers = new ArrayList<>();
        String URL = "turn:27.71.226.88:64001?transport=tcp";
        iceServers.add(PeerConnection.IceServer.builder(URL).setUsername("kalapa").setPassword("kalapa123456").createIceServer());
        PeerConnection.RTCConfiguration rtcConfig = new PeerConnection.RTCConfiguration(iceServers);
        rtcConfig.iceTransportsType = PeerConnection.IceTransportsType.RELAY;
        rtcConfig.sdpSemantics = PeerConnection.SdpSemantics.UNIFIED_PLAN;

        PeerConnection.Observer pcObserver = new ld();
        peerConnection = factory.createPeerConnection(rtcConfig, pcObserver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        livenessMessage = SDKUtils.ERROR_CODE.CANCELED;
        livenessResultCode = SDKUtils.LIVENESS_CODE.UNVERIFIED;
        intentResultCode = RESULT_CANCELED;
        onPreDestroy();
    }

    private void initializeDataChanel() {
        dataChannel = peerConnection.createDataChannel("chat", new DataChannel.Init());
        dataChannel.registerObserver(new DataChannel.Observer() {
            @Override
            public void onBufferedAmountChange(long l) {
                Log.d(TAG, "Data Observer: onBufferedAmountChange " + l);
            }

            @Override
            public void onStateChange() {
                Log.d(TAG, "Data Observer: onStateChange ");
            }

            @Override
            public void onMessage(DataChannel.Buffer buffer) {
                ByteBuffer data = buffer.data;
                byte[] bytes = new byte[data.remaining()];
                data.get(bytes);
                String command = new String(bytes);
                try {
                    command = command.split("&&")[1].replaceAll("'", "\"");
                    Log.d(TAG, "Data Observer: onMessage " + command);
                    JSONObject jsonCmd = new JSONObject(command);
                    Integer status = jsonCmd.getInt("status");
                    String message = jsonCmd.getString("message");
                    Log.d(TAG, "Data Observer: onMessage " + status + " - " + message);
                    if (status == SDKUtils.LIVENESS_CODE.EXPIRED) {

                    } else if (status == SDKUtils.LIVENESS_CODE.VERIFIED || status == SDKUtils.LIVENESS_CODE.FAILED) {
                        // Succeed
                        String base64_image = jsonCmd.getString("base64_image");
                        if (!base64_image.isEmpty()) {
                            byte[] decodedString = Base64.decode(base64_image, Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            FileUtils.saveFile(LivenessActivity.this, decodedByte, faceFileName);
                        }
                    }
                    runOnUiThread(() -> {
                        if (status == SDKUtils.LIVENESS_CODE.VERIFIED || status == SDKUtils.LIVENESS_CODE.FAILED || status == SDKUtils.LIVENESS_CODE.EXPIRED) {
                            binding.surfaceView.setVisibility(View.INVISIBLE);
//                            binding.frCamera.setVisibility(View.GONE);
                            binding.ivSelfieResult.setVisibility(View.VISIBLE);
                            binding.btnHolder.setVisibility(View.VISIBLE);
                            binding.btnCancel.setVisibility(status == SDKUtils.LIVENESS_CODE.VERIFIED ? View.GONE : View.VISIBLE);
                            binding.btnConfirm.setVisibility(status != SDKUtils.LIVENESS_CODE.VERIFIED ? View.GONE : View.VISIBLE);
                            if (status == SDKUtils.LIVENESS_CODE.VERIFIED || status == SDKUtils.LIVENESS_CODE.FAILED)
                                binding.ivSelfieResult.setImageBitmap(BitmapFactory.decodeFile(FileUtils.getFaceFile(LivenessActivity.this).getAbsolutePath()));
                            else {
                                binding.ivSelfieResult.setVisibility(View.INVISIBLE);
                            }
                        }
                        binding.ivGuideIcon.setImageResource(SDKUtils.setIconGuide(message));
                        binding.tvDataMessage.setText(SDKUtils.getLivenessMessage(status, message));
                    });
                } catch (
                        JSONException e) {
                    Log.e(TAG, e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void start() {
        isTimedOut = false;
        initializeSurfaceViews();
        // Create Peer Connection Factory
        binding.frCamera.setVisibility(View.VISIBLE);
        binding.surfaceView.setVisibility(View.VISIBLE);
        binding.ivSelfieResult.setVisibility(View.INVISIBLE);
        binding.btnHolder.setVisibility(View.INVISIBLE);
        initializePeerConnectionFactory();
        // Create Video Track and add it to factory, also show it
        createVideoTrackFromCameraAndShowIt();
        // Create Peer Connection
        initializePeerConnections();
        // Create Data Channel
        initializeDataChanel();
        // Negotiate
        startStreamingVideo();
        negotiate();
    }

    private void startStreamingVideo() {

        localStream = factory.createLocalMediaStream("Android_Local_Stream_" + System.currentTimeMillis());
        localStream.addTrack(videoTrackFromCamera);
//        localStream.addTrack(localAudioTrack);
//        peerConnection.addStream(localStream);
        for (VideoTrack track : localStream.videoTracks) {
            peerConnection.addTrack(track);
        }
    }

    private void negotiate() {
        MediaConstraints sdpMediaConstraints = new MediaConstraints();
        sdpMediaConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"));
        SdpObserver sdpObserver = new SdpObserver() {
            @Override
            public void onCreateSuccess(SessionDescription sessionDescription) {
                peerConnection.setLocalDescription(this, sessionDescription);
//                Log.d(TAG, "onCreateSuccess: " + peerConnection.getLocalDescription().description);
            }

            @Override
            public void onSetSuccess() {
                Log.d(TAG, "onSetSuccess");
            }

            @Override
            public void onCreateFailure(String s) {
                Log.d(TAG, "onCreateFailure: " + s);

            }

            @Override
            public void onSetFailure(String s) {
                Log.d(TAG, "onSetFailure: " + s);
            }
        };

        peerConnection.createOffer(sdpObserver, sdpMediaConstraints);
        // Send Offer
    }

    private void createVideoTrackFromCameraAndShowIt() {
//        audioConstraints = new MediaConstraints();
        videoCapturer = createVideoCapturer();
        SurfaceTextureHelper surfaceTextureHelper = SurfaceTextureHelper.create("CaptureThread", rootEglBase.getEglBaseContext());
        VideoSource videoSource = factory.createVideoSource(videoCapturer.isScreencast());
        videoCapturer.initialize(surfaceTextureHelper, this, videoSource.getCapturerObserver());
        videoCapturer.startCapture(VIDEO_RESOLUTION_WIDTH, VIDEO_RESOLUTION_HEIGHT, FPS);

        videoTrackFromCamera = factory.createVideoTrack(VIDEO_TRACK_ID, videoSource);
        videoTrackFromCamera.setEnabled(true);
        videoTrackFromCamera.addSink(binding.surfaceView);

        //create an AudioSource instance if needed
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    private VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer;
        if (useCamera2()) {
            videoCapturer = createCameraCapturer(new Camera2Enumerator(this));
        } else {
            videoCapturer = createCameraCapturer(new Camera1Enumerator(true));
        }
        return videoCapturer;
    }

    private class ld implements PeerConnection.Observer {
        @Override
        public void onAddTrack(RtpReceiver rtpReceiver, MediaStream[] mediaStreams) {
            Log.d(TAG, "PC Observer: onAddTrack " + rtpReceiver.track().state() + " -  " + mediaStreams.length);
        }

        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {
            Log.d(TAG, "PC Observer: onSignalingChange" + signalingState);
        }

        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
            Log.d(TAG, "PC Observer: onIceConnectionChange " + iceConnectionState);
        }

        @Override
        public void onIceConnectionReceivingChange(boolean b) {
            Log.d(TAG, "PC Observer: onIceConnectionReceivingChange " + b);
        }

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
            Log.d(TAG, "PC Observer: onIceGatheringChange " + iceGatheringState);
            if (iceGatheringState == PeerConnection.IceGatheringState.COMPLETE) {
                startNegotiate(peerConnection.getLocalDescription());
            }
        }

        @Override
        public void onIceCandidate(IceCandidate iceCandidate) {
            Log.d(TAG, "PC Observer: onIceCandidate " + iceCandidate);
            peerConnection.addIceCandidate(iceCandidate);
            JSONObject message = new JSONObject();
            try {
                message.put("type", "candidate");
                message.put("label", iceCandidate.sdpMLineIndex);
                message.put("id", iceCandidate.sdpMid);
                message.put("candidate", iceCandidate.sdp);
                Log.d(TAG, "onIceCandidate: sending candidate " + message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onIceCandidatesRemoved(IceCandidate[] iceCandidates) {
            Log.d(TAG, "PC Observer: onIceCandidatesRemoved " + iceCandidates);
        }

        @Override
        public void onAddStream(MediaStream mediaStream) {
            Log.d(TAG, "PC Observer: on Add Stream ");
        }

        @Override
        public void onRemoveStream(MediaStream mediaStream) {
            Log.d(TAG, "PC Observer: onRemoveStream " + mediaStream);
        }

        @Override
        public void onDataChannel(DataChannel dataChannel) {
            Log.d(TAG, "PC Observer: onDataChannel " + dataChannel.state());
        }

        @Override
        public void onRenegotiationNeeded() {
            Log.d(TAG, "PC Observer: onRenegotiationNeeded");
        }
    }
}
