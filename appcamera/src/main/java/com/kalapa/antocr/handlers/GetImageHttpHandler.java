package com.kalapa.antocr.handlers;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class GetImageHttpHandler extends AsyncTask<String, Void, byte[]> {
    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor())
            .build();
    Context c;

    public GetImageHttpHandler(Context context) {
        this.c = context;
    }

    @Override
    protected byte[] doInBackground(String... params) {
        Request.Builder builder = new Request.Builder();
        builder.url(params[0]);
        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            return response.body().bytes();
        } catch (Exception e) {
//            e.printStackTrace();
            Toast.makeText(c, "Connect Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return null;
    }
}
