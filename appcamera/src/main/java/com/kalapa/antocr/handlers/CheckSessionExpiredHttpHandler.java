package com.kalapa.antocr.handlers;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class CheckSessionExpiredHttpHandler extends AsyncTask<String, Void, Boolean> {
    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor())
            .build();
    Context c;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public CheckSessionExpiredHttpHandler(Context context) {
        this.c = context;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try {
            String session = params[0];
            String url = "https://dev-tung.kalapa.vn/api/kyc/ping";
            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .header("Authorization", session)
                    .build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return true;
            }
        } catch (Exception e) {
//            e.printStackTrace();
            Toast.makeText(c, "Connect Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
