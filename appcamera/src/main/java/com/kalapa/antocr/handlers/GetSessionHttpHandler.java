package com.kalapa.antocr.handlers;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class GetSessionHttpHandler extends AsyncTask<String, Void, String> {
    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor())
            .build();
    Context c;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public GetSessionHttpHandler(Context context) {
        this.c = context;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            JSONObject json = new JSONObject();
            json.put("app_token", params[0]);
            RequestBody body = RequestBody.create(JSON, json.toString()); // new
            String url = "https://dev-tung.kalapa.vn/api/auth/get-token";
            System.out.println("Json Body: " + json.toString());
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            System.out.println("");
            return response.body().string();
        } catch (Exception e) {
//            e.printStackTrace();
            Toast.makeText(c, "Connect Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return null;
    }
}
