package com.kalapa.antocr.handlers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kalapa.antocr.model.OfferRequest;
import com.kalapa.antocr.model.OfferResponse;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class SendOfferHttpHandler extends AsyncTask<String, Void, OfferResponse> {
    private final String TAG = SendOfferHttpHandler.class.getName();
    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor())
            .build();
    Context c;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public SendOfferHttpHandler(Context context) {
        this.c = context;
    }

    @Override
    protected OfferResponse doInBackground(String... params) {
        try {
            String url = "https://dev-tung.kalapa.vn/liveness/offer"; // Test luồng luôn xác thực 2 lần.
            OfferRequest offerRequest = new OfferRequest(params[0], params[1].toLowerCase(), params[2], "none", true, "2",true);
            RequestBody body = RequestBody.create(new Gson().toJson(offerRequest), JSON); // new
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            return new Gson().fromJson(response.body().string(), OfferResponse.class);
        } catch (Exception e) {
            Toast.makeText(c, "Connect Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return null;
    }
}
