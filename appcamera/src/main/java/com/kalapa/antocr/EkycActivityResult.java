package com.kalapa.antocr;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.kalapa.antocr.components.AnomalyListViewAdapter;
import com.kalapa.antocr.components.DecisionListViewAdapter;
import com.kalapa.antocr.components.ItemListViewAdapter;
import com.kalapa.antocr.databinding.ActivityEkycResultBinding;
import com.kalapa.antocr.model.AntifraudFull;
import com.kalapa.antocr.model.CardInfo;
import com.kalapa.antocr.model.DataVerify;
import com.kalapa.antocr.model.DecisionSummarize;
import com.kalapa.antocr.model.FaceVerifyInfo;
import com.kalapa.antocr.model.ItemView;
import com.kalapa.antocr.model.VerifyOCR;
import com.kalapa.antocr.utils.Common;
import com.kalapa.antocr.utils.SDKUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class EkycActivityResult extends AppCompatActivity {
    Socket mSocket;

    ActivityEkycResultBinding binding;
    private static final String TAG = EkycActivityResult.class.getName();
    String sessionObject;
    String selfieUrl;
    String frontUrl;
    String ocrResult;
    String antifraudResult;
    String decisionResult;
    String dataVerifyResult;
    String faceVerifyResult;
    ArrayList<ItemView> idCardInfoList = new ArrayList<>();
    ArrayList<ItemView> antifraudList = new ArrayList<>();
    ArrayList<ItemView> decisionDetailList = new ArrayList<>();
    private int dataVerifyRetry = 0;
    private int decisionRetry = 0;
    private int antifraudRetry = 0;
    String ekycMessage = "";
    int intentResultCode = RESULT_CANCELED;
    int ekycResultCode = SDKUtils.LIVENESS_CODE.UNVERIFIED;
    private static final int LIVENESS_CODE = 101;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEkycResultBinding.inflate(getLayoutInflater());
        if (getSupportActionBar() != null) getSupportActionBar().hide();
        setContentView(binding.getRoot());
        Intent intent = getIntent();
        sessionObject = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_ID);
        onPreExecute();

//        onUIUpdate();
    }


    private void onExecute() {
        AntOcr.init(sessionObject, SessionManager.getHost());
        Intent intent = getIntent();
        String selfieUrl = intent.getStringExtra(SDKUtils.SDK_CONFIG.LIVENESS_URI);
        String frontUrl = intent.getStringExtra(SDKUtils.SDK_CONFIG.OCR_FRONT_URI);
        String ocrResult = intent.getStringExtra(SDKUtils.SDK_CONFIG.OCR_RESULT);
        CardInfo cardInfo = new Gson().fromJson(ocrResult, CardInfo.class);
        if (cardInfo != null && cardInfo.getData() != null) {
            binding.pbCardInfo.setVisibility(View.GONE);
            if (cardInfo.getData().getFields().getId_number() != null && !cardInfo.getData().getFields().getId_number().isEmpty())
                idCardInfoList.add(new ItemView("Số giấy tờ tùy thân", cardInfo.getData().getFields().getId_number()));
            if (cardInfo.getData().getFields().getName() != null && !cardInfo.getData().getFields().getName().isEmpty())
                idCardInfoList.add(new ItemView("Họ tên", cardInfo.getData().getFields().getName()));
            if (cardInfo.getData().getFields().getBirthday() != null && !cardInfo.getData().getFields().getBirthday().isEmpty())
                idCardInfoList.add(new ItemView("Ngày sinh", cardInfo.getData().getFields().getBirthday()));
            if (cardInfo.getData().getFields().getGender() != null && !cardInfo.getData().getFields().getGender().isEmpty())
                idCardInfoList.add(new ItemView("Giới tính", cardInfo.getData().getFields().getGender()));
            if (cardInfo.getData().getFields().getHome() != null && !cardInfo.getData().getFields().getHome().isEmpty())
                idCardInfoList.add(new ItemView("Nguyên Quán", cardInfo.getData().getFields().getHome()));
            if (cardInfo.getData().getFields().getResident() != null && !cardInfo.getData().getFields().getResident().isEmpty())
                idCardInfoList.add(new ItemView("Hộ Khẩu", cardInfo.getData().getFields().getResident()));
            if (cardInfo.getData().getFields().getDoe() != null && !cardInfo.getData().getFields().getDoe().isEmpty())
                idCardInfoList.add(new ItemView("Ngày hết hạn", cardInfo.getData().getFields().getDoe()));
            if (cardInfo.getData().getFields().getDoi() != null && !cardInfo.getData().getFields().getDoi().isEmpty())
                idCardInfoList.add(new ItemView("Ngày làm giấy tờ", cardInfo.getData().getFields().getDoi()));
            if (cardInfo.getData().getFields().getPoi() != null && !cardInfo.getData().getFields().getPoi().isEmpty())
                idCardInfoList.add(new ItemView("Nơi làm giấy tờ", cardInfo.getData().getFields().getPoi()));
            if (cardInfo.getData().getFields().getFeatures() != null && !cardInfo.getData().getFields().getFeatures().isEmpty())
                idCardInfoList.add(new ItemView("Đặc điểm nhận dạng", cardInfo.getData().getFields().getFeatures()));
            if (cardInfo.getData().getFields().getType() != null && !cardInfo.getData().getFields().getType().isEmpty())
                idCardInfoList.add(new ItemView("Loại giấy tờ", cardInfo.getData().getFields().getType()));
            ListView lv = findViewById(R.id.lv_card_info);
            ItemListViewAdapter idCardListViewAdapter = new ItemListViewAdapter(idCardInfoList);
            lv.setAdapter(idCardListViewAdapter);
            Common.arrangeHeightListView(lv, true);
        } else {
            View ocrHolder = findViewById(R.id.layout_ocr_holder);
            ocrHolder.setVisibility(View.GONE);
        }
        binding.ivFront.setImageBitmap(BitmapFactory.decodeFile(frontUrl));
        binding.ivSelfie.setImageBitmap(BitmapFactory.decodeFile(selfieUrl));
        Button btnFinish = findViewById(R.id.btn_finish);
        btnFinish.setOnClickListener((v) -> {
            preDestroy();
        });
        AntOcr.getAntifraudFull(new AntOcrCallback<AntifraudFull>() {
            @Override
            public void onOk(AntifraudFull data) {
                binding.pbAntifraud.setVisibility(View.GONE);
                Log.d(TAG, "AntifraudFull Result Try:  " + antifraudRetry + " - " + new Gson().toJson(data));
                antifraudResult = new Gson().toJson(data);
                if (data.getResults().getBlacklist() != null) {
                    antifraudList.add(new ItemView("Blacklist", data.getResults().getBlacklist() ? "Không đạt" : "Đạt", data.getResults().getBlacklist()));
                }
                if (data.getResults().getAbnormal_face() != null && data.getResults().getAbnormal_face().getPrediction() != null) {
                    antifraudList.add(new ItemView("Khuôn mặt bất thường", data.getResults().getAbnormal_face().getPrediction() ? "Không đạt" : "Đạt", data.getResults().getAbnormal_face() != null && data.getResults().getAbnormal_face().getPrediction()));
                }
                if (data.getResults().getFaces_matched() != null && data.getResults().getFaces_matched().getCount() != null) {
                    antifraudList.add(new ItemView("Trùng khuôn mặt", String.valueOf(data.getResults().getFaces_matched().getCount()), data.getResults().getFaces_matched() != null && data.getResults().getFaces_matched().getCount() > 0));
                }
                if (data.getResults().getRecapture() != null && data.getResults().getRecapture().getPrediction() != null) {
                    antifraudList.add(new ItemView("Ảnh chụp màn hình hoặc Photocopy", data.getResults().getRecapture().getPrediction() ? "Không đạt" : "Đạt", data.getResults().getRecapture() != null && data.getResults().getRecapture().getPrediction()));
                }
                if (data.getResults().getCorner_cut() != null && data.getResults().getCorner_cut().getPrediction() != null) {
                    antifraudList.add(new ItemView("Ảnh bị cắt góc", data.getResults().getCorner_cut().getPrediction() ? "Không đạt" : "Đạt", data.getResults().getCorner_cut() != null && data.getResults().getCorner_cut().getPrediction()));
                }
                if (data.getResults().getAbnormal_text() != null
                        && data.getResults().getAbnormal_text().getPrediction() != null) {
                    antifraudList.add(new ItemView("Font chữ bất thường", data.getResults().getAbnormal_text().getPrediction() ? "Không đạt" : "Đạt", data.getResults().getAbnormal_text() != null && data.getResults().getAbnormal_text().getPrediction()));
                }
                if (data.getResults().getId_font() != null && data.getResults().getId_font().getPrediction() != null) {
                    antifraudList.add(new ItemView("Font ID bất thường", data.getResults().getId_font().getPrediction() ? "Không đạt" : "Đạt", data.getResults().getId_font() != null && data.getResults().getId_font().getPrediction()));
                }
                if (data.getResults().getMark() != null && data.getResults().getMark().getPrediction() != null) {
                    antifraudList.add(new ItemView("Dấu nổi bất thường", data.getResults().getMark().getPrediction() ? "Không đạt" : "Đạt", data.getResults().getMark() != null && data.getResults().getMark().getPrediction()));
                }
                ListView lv = findViewById(R.id.lv_antifraud_info);
                if (antifraudList != null && antifraudList.size() > 0) {
                    AnomalyListViewAdapter anomalyListViewAdapter = new AnomalyListViewAdapter(antifraudList);
                    lv.setAdapter(anomalyListViewAdapter);
                    Common.arrangeHeightListView(lv, false);
                } else {
                    View antifraudHolder = findViewById(R.id.layout_antifraud_holder);
                    antifraudHolder.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNG(Exception e) {
                onDataGetError(e);
                View antifraudHolder = findViewById(R.id.layout_antifraud_holder);
                antifraudHolder.setVisibility(View.GONE);
            }
        });

        AntOcr.getFaceVerify(new AntOcrCallback<FaceVerifyInfo>() {
            @Override
            public void onOk(FaceVerifyInfo data) {
                Log.d(TAG, "FaceVerifyInfo Result " + new Gson().toJson(data));
                boolean isMatched = data.getData() != null && data.getData().getIs_matched();
                faceVerifyResult = new Gson().toJson(data);
                ((TextView) findViewById(R.id.tv_face_match)).setText(isMatched ? "Trùng khớp" : "Không trùng khớp");
                ((TextView) findViewById(R.id.tv_face_match)).setTextColor(isMatched ? getResources().getColor(R.color.ekyc_green) : getResources().getColor(R.color.ekyc_red));
                ((ImageView) findViewById(R.id.iv_face_match)).setImageResource(isMatched ? R.drawable.ok : R.drawable.not_ok);
            }

            @Override
            public void onNG(Exception e) {
                onDataGetError(e);
            }
        });

        AntOcr.getVerifyData(new AntOcrCallback<DataVerify>() {
            @Override
            public void onOk(DataVerify data) {
                Log.d(TAG, "DataVerify Result Try: " + dataVerifyRetry + " - " + new Gson().toJson(data));
                if (data.getIdCardInfo() == null && dataVerifyRetry < 3) {
                    dataVerifyRetry++;
                    new Handler().postDelayed(() -> {
                        AntOcr.getVerifyData(this);
                    }, 10000);
                } else if (data.getIdCardInfo() != null) {
                    dataVerifyResult = new Gson().toJson(data);
                    if (data.getVerified() != null) {
                        ((TextView) findViewById(R.id.tv_verify_data)).setText(data.getVerified() ? "Đã xác thực" : "Chưa xác thực");
                        ((TextView) findViewById(R.id.tv_verify_data)).setTextColor(getResources().getColor(data.getVerified() ? R.color.ekyc_green : R.color.ekyc_red));
                        ((ImageView) findViewById(R.id.iv_verify_data)).setImageResource(data.getVerified() ? R.drawable.ok : R.drawable.not_ok);
                    } else {
                        ((TextView) findViewById(R.id.tv_verify_data)).setText("N/A");
                        ((ImageView) findViewById(R.id.iv_verify_data)).setImageResource(R.drawable.ic_minus_98);
                    }
                }
            }

            public void onNG(Exception e) {
                onDataGetError(e);
            }
        });

        AntOcr.getDecisionDetail(new AntOcrCallback<DecisionSummarize>() {
            @Override
            public void onOk(DecisionSummarize data) {
                Log.d(TAG, "Decision Data Result Try: " + data + " - " + new Gson().toJson(data));
                if (data.getDecision() == null && decisionRetry < 3) {
                    decisionRetry++;
                    new Handler().postDelayed(() -> {
                        AntOcr.getDecisionDetail(this);
                    }, 10000);
                } else if (data.getDetails() != null && data.getDetails().size() > 0) {
                    binding.pbDecision.setVisibility(View.GONE);
                    decisionResult = new Gson().toJson(data);
                    // Get occurred shit here.
                    binding.tvDecision.setText(data.getDecision().equals("MANUAL") ? "ĐANG XÉT DUYỆT" : data.getDecision().equals("APPROVED") ? "CHẤP NHẬN" : "TỪ CHỐI");
                    binding.tvDecision.setTextColor(data.getDecision().equals("MANUAL") ? getResources().getColor(R.color.ocr_colorAccent) :
                            data.getDecision().equals("APPROVED") ? getResources().getColor(R.color.ekyc_green) : getResources().getColor(R.color.ekyc_red));

                    for (DecisionSummarize.DecisionDetailObj detail : data.getDetails()) {
                        if (detail.getIsPass() < 0) {
                            Log.d(TAG, "Add Detail: " + detail);
                            try {
                                decisionDetailList.add(new ItemView(detail.getDescription_vi(),
                                        detail.getIsPass() == 0 ? "Từ chối" : "Đang duyệt",
                                        detail.getIsPass() == 0 ? false : null));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (decisionDetailList != null && decisionDetailList.size() > 0) {
                        DecisionListViewAdapter decisionListViewAdapter = new DecisionListViewAdapter(decisionDetailList);
                        binding.lvDecisionDetail.setAdapter(decisionListViewAdapter);
                        Common.arrangeHeightListView(binding.lvDecisionDetail, false);
                    }
                    Log.d(TAG, "Decision Details: " + decisionDetailList);

                } else {
                    binding.decisionLayerHolder.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNG(Exception e) {

            }
        });
    }

    private void onDataGetError(Exception e) {
        e.printStackTrace();
        Log.d(TAG, "get Data Error " + e.getMessage());
    }

    private void onUIUpdate() {

    }


    private void onPreExecute() {
        Intent intent = getIntent();
        if (sessionObject != null) {
            onExecute();
        } else {
            ekycMessage = SDKUtils.ERROR_CODE.NETWORK_ERROR;
            ekycResultCode = SDKUtils.OCR_CODE.OTHER_ERROR;
            intentResultCode = RESULT_CANCELED;
            preDestroy();
        }
    }


    private void preDestroy() {
        Intent intent = getIntent();
        intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT_CODE, ekycResultCode);
        intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT, ocrResult);
        intent.putExtra(SDKUtils.SDK_CONFIG.EKYC_SELFIE_RESULT, faceVerifyResult);
        intent.putExtra(SDKUtils.SDK_CONFIG.EKYC_VERIFY_RESULT, dataVerifyResult);
        intent.putExtra(SDKUtils.SDK_CONFIG.EKYC_DECISION_RESULT, decisionResult);
        intent.putExtra(SDKUtils.SDK_CONFIG.EKYC_ANTIFRAUD_RESULT, antifraudResult);
        setResult(intentResultCode, intent);
        finish();
    }
}
