package com.kalapa.antocr;

import static com.kalapa.antocr.SessionManager.checkIfSessionExpired;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.kalapa.antocr.databinding.ActivityOcrIdCardBinding;
import com.kalapa.antocr.model.CapturedPicture;
import com.kalapa.antocr.model.CardInfo;
import com.kalapa.antocr.model.PictureType;
import com.kalapa.antocr.model.SessionObject;
import com.kalapa.antocr.utils.SDKUtils;

import java.io.File;
import java.util.List;


public class IdCardOcrActivity extends AppCompatActivity {
    ActivityOcrIdCardBinding binding;
    private static final String TAG = IdCardOcrActivity.class.getName();
    String idCardOcrResult;
    int idCardOcrCode;
    int intentResult;
    String sessionObject;
    CardInfo cardInfo;
    CardInfo frontInfo;
    CardInfo backInfo;
    String frontUrl;
    String backUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOcrIdCardBinding.inflate(getLayoutInflater());
        if(getSupportActionBar() != null) getSupportActionBar().hide();
        setContentView(binding.getRoot());
        onPreExecute();
        onUIUpdate();
    }

    private void updateIntent(String key, String value) {
        Intent i = getIntent();
        i.putExtra(key, value);
    }

    private void onExecute() {
        AntOcr.init(sessionObject, SessionManager.getHost());
        binding.btnTakeFront.setOnClickListener((v) -> {
            AntOcr.startScan(IdCardOcrActivity.this, PictureType.CARD_FRONT, new AntOcrCallback<List<CapturedPicture>>() {
                @Override
                public void onOk(List<CapturedPicture> data) {
                    if (data != null && data.size() > 0) {
                        frontUrl = data.get(0).getFile().getAbsolutePath();
                        updateIntent(SDKUtils.SDK_CONFIG.OCR_FRONT_URI, frontUrl);
                        binding.ivFront.setImageBitmap(BitmapFactory.decodeFile(frontUrl));
                        binding.tvResultFront.setText("Đang bóc tách thông tin, vui lòng chờ...");
                        // Call get id
                        onFrontDataProcess(frontUrl);
                    } else {
                        binding.tvResultFront.setText("Capture error, no image has been captured");
                    }
                }

                @Override
                public void onNG(Exception e) {
                    binding.tvResultFront.setText(e.getMessage());
                }
            });
        });

        binding.btnTakeBack.setOnClickListener((v) -> {
            AntOcr.startScan(IdCardOcrActivity.this, PictureType.CARD_BACK, new AntOcrCallback<List<CapturedPicture>>() {
                @Override
                public void onOk(List<CapturedPicture> data) {
                    if (data != null && data.size() > 0) {
                        backUrl = data.get(0).getFile().getAbsolutePath();
                        updateIntent(SDKUtils.SDK_CONFIG.OCR_BACK_URI, backUrl);
                        binding.ivBack.setImageBitmap(BitmapFactory.decodeFile(backUrl));
                        binding.tvResultBack.setText("Đang bóc tách thông tin, vui lòng chờ...");
                        // Call get id
                        onBackDataProcess(backUrl);
                    } else {
                        binding.tvResultBack.setText("Capture error, no image has been captured");
                    }
                }

                @Override
                public void onNG(Exception e) {
                    binding.tvResultBack.setText(e.getMessage());
                    idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR;
                    idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResult = RESULT_CANCELED;
                    preDestroy();
                }
            });
        });

        binding.btnContinue.setOnClickListener((v) -> {
            if (frontUrl != null && backUrl != null
                    && frontInfo != null
                    && frontInfo.getError() != null
                    && frontInfo.getError().getCode() == 0
                    && frontInfo.getData() != null
                    && frontInfo.getData().getCard_type() != null
                    && backInfo != null
                    && backInfo.getError() != null
                    && backInfo.getError().getCode() == 0
                    && backInfo.getData() != null
                    && backInfo.getData().getCard_type() != null
            ) {
                String frontCard = frontInfo.getData().getCard_type().split("_")[0];
                String backCard = backInfo.getData().getCard_type().split("_")[0];
                if (frontCard.trim().equals(backCard.trim())) {
                    idCardOcrCode = SDKUtils.OCR_CODE.SUCCESS;
                    intentResult = RESULT_OK;
                    preDestroy();
                } else {
                    Toast.makeText(IdCardOcrActivity.this, "Vui lòng chụp ảnh cùng một loại giấy tờ", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(IdCardOcrActivity.this, "Vui lòng chụp ảnh hợp lệ trước khi tiếp tục", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void onFrontDataProcess(String frontUri) {
        File file = new File(frontUri);
        if (file.exists()) {
            AntOcr.getFrontCard(file, new AntOcrCallback<CardInfo>() {
                @Override
                public void onOk(CardInfo data) {
                    // Check message
                    // Check type
                    if (data != null) {
                        if (data.getError().getCode() == 0) {
                            // Vào việc
                            binding.tvResultFront.setText("Ảnh hợp lệ");
                            binding.tvResultFront.setTextColor(getResources().getColor(R.color.ekyc_green));
                            frontInfo = data;
                            if (cardInfo == null)
                                cardInfo = data;
                            else {
                                // Set info
                                try {
                                    cardInfo.mergeInformation(data.getData());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            idCardOcrResult = new Gson().toJson(cardInfo);
                        } else {
                            assert data.getError() != null;
                            binding.tvResultFront.setText(data.getError().getMessage());
                            binding.tvResultFront.setTextColor(getResources().getColor(R.color.ekyc_red));
                        }
                    } else {
                        // Không có data . rarely
                        idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR;
                        idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                        intentResult = RESULT_CANCELED;
                        preDestroy();
                    }
                    binding.tvIdCardOcr.setText(idCardOcrResult);
                    onUIUpdate();
                }

                @Override
                public void onNG(Exception e) {
                    idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR + e.getMessage();
                    idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResult = RESULT_CANCELED;
                    preDestroy();
                }
            });
        }
    }


    private void onBackDataProcess(String backUri) {
        File file = new File(backUri);
        if (file.exists()) {
            AntOcr.getBackCard(file, new AntOcrCallback<CardInfo>() {
                @Override
                public void onOk(CardInfo data) {
                    // Check message
                    // Check type
                    if (data != null) {
                        if (data.getError().getCode() == 0) {
                            // Vào việc
                            binding.tvResultBack.setText("Ảnh hợp lệ");
                            binding.tvResultBack.setTextColor(getResources().getColor(R.color.ekyc_green));
                            backInfo = data;
                            if (cardInfo == null)
                                cardInfo = data;
                            else {
                                // Set info
                                try {
                                    cardInfo.mergeInformation(data.getData());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            idCardOcrResult = new Gson().toJson(cardInfo);
                        } else {
                            assert data.getError() != null;
                            binding.tvResultBack.setText(data.getError().getMessage());
                            binding.tvResultBack.setTextColor(getResources().getColor(R.color.ekyc_red));
                        }
                    } else {
                        // Không có data . rarely
                        idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR;
                        idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                        intentResult = RESULT_CANCELED;
                        preDestroy();
                    }
                    binding.tvIdCardOcr.setText(idCardOcrResult);
                    onUIUpdate();
                }

                @Override
                public void onNG(Exception e) {
                    idCardOcrResult = SDKUtils.ERROR_CODE.UNEXPECTED_ERROR + e.getMessage();
                    idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResult = RESULT_CANCELED;
                    preDestroy();
                }
            });
        }
    }

    private void onUIUpdate() {
        if (frontUrl == null || backUrl == null) {
//            binding.btnContinue.setClickable(false);
//            binding.btnContinue.setBackgroundColor(getResources().getColor(R.color.ekyc_grey));
        } else {
//            binding.btnContinue.setBackgroundColor(getResources().getColor(R.color.ekyc_btn_color));
//            binding.btnContinue.setClickable(true);
        }
    }


    private void onPreExecute() {
        Intent intent = getIntent();
        String token = intent.getStringExtra(SDKUtils.SDK_CONFIG.TOKEN);
        String session = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_ID);
        String DEFAULT_PLAN =intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_PLAN);
        Boolean SHOW_ALL_RESULT = intent.getBooleanExtra(SDKUtils.SDK_CONFIG.SESSION_SHOW_ALL_RESULT,false);
        String CALLBACK_URL = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_CALLBACK_URL);
        String UPLOAD_TYPE = intent.getStringExtra(SDKUtils.SDK_CONFIG.SESSION_UPLOAD_TYPE);
        if (session != null) {
            if (checkIfSessionExpired(IdCardOcrActivity.this, session)) {
                Log.d(TAG, "Old Session is still  Ok. " + session);
                sessionObject = session;
                onExecute();
            } else {
                // Session is expired
                idCardOcrResult = SDKUtils.ERROR_CODE.SESSION_EXPIRED;
                idCardOcrCode = SDKUtils.OCR_CODE.EXPIRED;
                intentResult = RESULT_CANCELED;
                preDestroy();
            }
        } else {
            new SessionManager(token).getSessionToken(this,DEFAULT_PLAN, SHOW_ALL_RESULT, UPLOAD_TYPE, CALLBACK_URL, new AntOcrCallback<SessionObject>() {
                @Override
                public void onOk(SessionObject data) {
                    Log.d(TAG, "Get new Session Ok. " + data.getToken());
                    sessionObject = data.getToken();
                    onExecute();
                }

                @Override
                public void onNG(Exception e) {
                    // Get Token Problem
                    e.printStackTrace();
                    idCardOcrResult = SDKUtils.ERROR_CODE.NETWORK_ERROR;
                    idCardOcrCode = SDKUtils.OCR_CODE.OTHER_ERROR;
                    intentResult = RESULT_CANCELED;
                    preDestroy();
                }
            });
        }

    }

    private void preDestroy() {
        Intent intent = getIntent();
        intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT_CODE, idCardOcrCode);
        intent.putExtra(SDKUtils.SDK_CONFIG.OCR_RESULT, idCardOcrResult);
        setResult(intentResult, intent);
        finish();
    }
}
