package com.kalapa.antocr.utils;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.kalapa.antocr.R;

import java.util.Random;

public class SDKUtils {
    public static final class ERROR_CODE {
        public static final String SUCCEED = "Thành công";
        public static final String CANCELED = "Hủy bỏ";
        public static final String SESSION_EXPIRED = "Phiên làm việc đã hết hạn, vui lòng thử lại sau";
        public static final String PERMISION_NOT_ALLOWED = "Ứng dụng cần sự cho phép về quyền Camera để có thể hoạt động được";
        public static final String UNEXPECTED_ERROR = "Đã xảy ra lỗi không mong muốn, vui lòng liên hệ bộ phận hỗ trợ và thử lại sau";
        public static final String NETWORK_ERROR = "Đã xảy ra lỗi trong đường truyền, vui lòng kiểm tra lại đường truyền mạng và thử lại sau";
    }

    public static class SDK_CONFIG {
        public static final String SESSION_ID = "SESSION_ID";
        public static final String SESSION_PLAN = "SESSION_PLAN";
        public static final String SESSION_CALLBACK_URL = "SESSION_CALLBACK_URL";
        public static final String SESSION_UPLOAD_TYPE = "SESSION_UPLOAD_TYPE";
        public static final String SESSION_SHOW_ALL_RESULT = "SESSION_SHOW_ALL_RESULT";

        public static final String TOKEN = "TOKEN";
        public static final String LIVENESS_RESULT = "LIVENESS_RESULT";
        public static final String LIVENESS_RESULT_CODE = "LIVENESS_RESULT_CODE";
        public static final String LIVENESS_URI = "LIVENESS_URI";

        public static final String OCR_RESULT = "OCR_RESULT_RESULT";
        public static final String OCR_RESULT_CODE = "OCR_RESULT_CODE";
        public static final String OCR_FRONT_URI = "OCR_FRONT_URI";
        public static final String OCR_BACK_URI = "OCR_BACK_URI";


        public static final String EKYC_ANTIFRAUD_RESULT = "EKYC_ANTIFRAUD_RESULT";
        public static final String EKYC_ANTIFRAUD_CODE = "EKYC_ANTIFRAUD_CODE";
        public static final String EKYC_DECISION_RESULT = "EKYC_DECISION_RESULT";
        public static final String EKYC_LOGIC_CODE = "EKYC_LOGIC_CODE";
        public static final String EKYC_VERIFY_RESULT = "EKYC_VERIFY_RESULT";
        public static final String EKYC_VERIFY_CODE = "EKYC_VERIFY_CODE";
        public static final String EKYC_SELFIE_RESULT = "EKYC_SELFIE_RESULT";
        public static final String EKYC_SELFIE_CODE = "EKYC_SELFIE_CODE";

        // CONFIG
        String SELECT_DOCUMENT = "SELECT_DOCUMENT";
        String SHOW_INSTRUCTION = "SHOW_INSTRUCTION";
        String LANGUAGE = "LANGUAGE";
        String CHANGE_COLOR = "CHANGE_COLOR";
        String CHANGE_TEXT_COLOR = "CHANGE_TEXT_COLOR";
        String ENABLE_SCAN_QR_NEW_CCCD = "ENABLE_SCAN_QR_NEW_CCCD"; // Not supported yet!
        String SHOW_RESULT = "SHOW_RESULT";
        // The rest is config at CMS like ULR_UPLOAD_IMAGE, URL_OCR, URL_FRONT, URL_SELFIE, URL_ANTIFRAUD, URL_LOGIC, URL_VERIFY, URL_SELFIE
    }

    public static int setIconGuide(String input) {
        switch (input) {
            case "HoldSteady3Second":
            case "HoldSteady2Second":
                return R.drawable.head_straight;
            case "TurnLeft":
                return R.drawable.head_left;
            case "TurnRight":
                return R.drawable.head_right;
            case "TurnUp":
                return R.drawable.liveness_head_up;
            case "TurnDown":
                return R.drawable.head_down;
            case "EyeBlink":
                return R.drawable.liveness_eye;
            case "ShakeHead":
                return new Random().nextInt(2) % 2 == 0 ? R.drawable.head_right : R.drawable.head_left;
            case "NodeHead":
                return R.drawable.head_down;
            case "Success":
                return R.drawable.result_success;
            case "Connecting":
                return R.drawable.head_straight;
            case "Initializing":
                return R.drawable.head_straight;
            case "ConnectionFailed":
                return R.drawable.status_bar_closed_default_background;
            default:
                return R.drawable.head_straight;
        }
    }

    public static String getLivenessMessage(Integer status, String input) {
        if (status != null && status == 2) {
            return "Phiên xác thực hết hạn, vui lòng thử lại";
        }
        switch (input) {
            case "HoldSteady3Second":
                return "Giữ đầu ngay ngắn, nhìn thẳng trong 3 giây";
            case "TurnLeft":
                return "Quay trái";
            case "TurnRight":
                return "Quay phải";
            case "TurnUp":
                return "Quay lên";
            case "TurnDown":
                return "Quay xuống";
            case "HoldSteady2Second":
                return "Giữ đầu ngay ngắn, nhìn thẳng trong 2 giây";
            case "EyeBlink":
                return "Nháy mắt";
            case "ShakeHead":
                return "Lắc đầu";
            case "NodeHead":
                return "Gật đầu";
            case "Success":
                return "Good!";
            case "Connecting":
                return "Đang kết nối...";
            case "Initializing":
                return "Đang khởi tạo...";
            case "Processing":
                if (status == 1)
                    return "Xác thực thành công";
                if (status == 3)
                    return "Xác thực thất bại";
                return "Đang xác thực...";
            case "ConnectionFailed":
                return "Kết nối thất bại";
            case "Finished":
                return "Hoàn thành!";
            default:
                return input;
        }
    }

    public static class LIVENESS_CODE {
        public static int UNVERIFIED = 0;
        public static int VERIFIED = 1;
        public static int EXPIRED = 2;
        public static int FAILED = 3;
        public static int OTHER_ERROR = 99;
    }

    public static class OCR_CODE {
        public static int SUCCESS = 0;
        public static int FAILED = 1;
        public static int EXPIRED = 2;
        public static int OTHER_ERROR = 99;
    }
}
