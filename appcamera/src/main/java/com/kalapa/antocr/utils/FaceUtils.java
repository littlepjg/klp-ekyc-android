package com.kalapa.antocr.utils;

import android.graphics.*;
import android.util.*;

import com.google.android.gms.vision.face.*;


public class FaceUtils {
    public Bitmap centerCrop(Bitmap original, SparseArray<Face> faces) {

        float left;
        float top;

        PointF focusPoint = new PointF();
        PointF size = new PointF();
        detectFace(original, faces, focusPoint, size);
        int width = (int) size.x;
        int height = (int) size.y + 120;
        left = focusPoint.x - (size.x / 2);
        top = focusPoint.y - (size.y / 2);

        if (width + left > original.getWidth()) {
            width = original.getWidth() - (int) left;
        }
        if (height + top > original.getHeight()) {
            height = original.getHeight() - (int) top;
        }

        return Bitmap.createBitmap(original, (int) left, (int) top, width, height);

    }

    /**
     * Calculates a point (focus point) in the bitmap, around which cropping needs to be performed.
     *
     * @param bitmap           Bitmap in which faces are to be detected.
     * @param centerOfAllFaces To store the center point.
     */
    private void detectFace(Bitmap bitmap, SparseArray<Face> faces, PointF centerOfAllFaces, PointF size) {
        if (faces == null || faces.size() <= 0) {
            centerOfAllFaces.set(bitmap.getWidth() / 2, bitmap.getHeight() / 2);
            size.x = bitmap.getWidth();
            size.y = bitmap.getHeight();
            return;
        } // center crop;
        final int totalFaces = faces.size();
        if (totalFaces > 0) {
            float sumX = 0f;
            float sumY = 0f;
            for (int i = 0; i < totalFaces; i++) {
                Face face = faces.get(faces.keyAt(i));
                size.x = Math.max(size.x, face.getWidth());
                size.y = Math.max(size.y, face.getHeight());
                PointF faceCenter = new PointF();
                getFaceCenter(face, faceCenter);
                sumX = sumX + faceCenter.x;
                sumY = sumY + faceCenter.y;
            }
            centerOfAllFaces.set(sumX / totalFaces, sumY / totalFaces);
            return;
        }
        centerOfAllFaces.set(bitmap.getWidth() / 2, bitmap.getHeight() / 2); // center crop
    }

    /**
     * Calculates center of a given face
     *
     * @param face   Face
     * @param center Center of the face
     */
    private void getFaceCenter(Face face, PointF center) {
        float x = face.getPosition().x;
        float y = face.getPosition().y;
        float width = face.getWidth();
        float height = face.getHeight();
        center.set(x + (width / 2), y + (height / 2)); // face center in original bitmap
    }

}