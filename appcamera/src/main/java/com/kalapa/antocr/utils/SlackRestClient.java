package com.kalapa.antocr.utils;


import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class SlackRestClient {
    public static final String CUONGVC = "<@UCMP7L5JA>";
    public static final String THIENDN = "<@UCM96SU02>";
    public static final String TUNG = "<@UCTJZQ13R>";
    public static final String DUCNH = "<@UCRUS5HFH>";
    public static final String NAMPTH = "<@UD9Q1304T>";
    public static final String GIANGNT = "<@UL44UJL84>";
    public static final String THANGPV = "<@UNM8VLA2C>";
    public static final String DUYPN = "<@UPLAJAX6U>";

    public static final String SLACK_WEBHOOKS_DEV_TU = "https://hooks.slack.com/services/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"; // dev_tu
    public static final String SLACK_WEBHOOKS_NOTI = "https://hooks.slack.com/services/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"; // noti
    //    private  static final String SLACK_WEBHOOKS_NOTI = "https://hooks.slack.com/services/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"; // dev_tu
    public static final String SLACK_WEBHOOKS_ERROR_NOTI = "https://hooks.slack.com/services/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"; // error

    private Retrofit retrofit;

    public SlackRestClient(String host) {
        createGateway(host);
    }

    private void createGateway(String host) {
        retrofit = new Retrofit.Builder()
                .baseUrl(host)
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private OkHttpClient getHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging).build();
    }

    public void sendMessage(String message) {
        ISlackRest iSlackRest = retrofit.create(ISlackRest.class);
        iSlackRest.sendMessageViaWebhook(message);
    }

    private interface ISlackRest {
        @POST("/")
        Observable<Response<Object>> sendMessageViaWebhook(
                @Body String text);
    }
}
