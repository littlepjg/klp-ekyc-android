package com.kalapa.antocr.utils;

import android.util.Log;

import org.webrtc.SessionDescription;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebRTCUtils {
    private static final String TAG = WebRTCUtils.class.getName();

    private static Matcher getFirstIfRegex(String input, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if (matcher.find())
            return matcher;
        return null;
    }

    //    m=video 64320 UDP/TLS/RTP/SAVPF 96 98 100 127 125 97 99 101 124
    //    ('video', "VP8/90000", offer.sdp)
    public static String sdpFilterCodec(String kind, String codec, SessionDescription realSdp) {
        List<String> allowed = new ArrayList<>();
        String rtxRegex = "a=fmtp:(\\d+) apt=(\\d+)\r$";
        String codecRegex = "a=rtpmap:(\\d+) " + codec;
//        Log.d(TAG, "SDP Filter Codec: Initialize codec regex " + codecRegex);
        boolean isKind = false;
        String[] lines = realSdp.description.split("\n");
        for (String line : lines) {
            if (line.startsWith("m=" + kind + " "))
                isKind = true;
            else if (line.startsWith("m="))
                isKind = false;
            // Chỉ accept m=video
            if (isKind) {
                Matcher matcher = getFirstIfRegex(line, codecRegex);
                if (matcher != null) {
                    allowed.add(matcher.group(1));
                    Log.d(TAG, "SDP Filter Codec: Add (" + matcher.group(1) + ") - Allowed: " + allowed.toString());
                }
                matcher = getFirstIfRegex(line, rtxRegex);
                if (matcher != null && allowed.contains(matcher.group(2))) {
                    allowed.add(matcher.group(1));
                    Log.d(TAG, "SDP Filter RTX: Add (" + matcher.group(1) + ") - Allowed: " + allowed.toString());
                }
            }
        }

        String skipRegex = "a=(fmtp|rtcp-fb|rtpmap):([0-9]+)";
        String videoRegex = "(m=" + kind + " .*?)( ([0-9]+))*\\s*$";
        String sdp = "";
        isKind = false;
        String allowedStr = "";
        for (String a : allowed) {
            allowedStr += a + " ";
//            Log.d(TAG, "SDP Filter Codec: allowed Str " + allowedStr);
        }
        allowedStr = allowedStr.trim();
        for (String line : lines) {
            if (line.startsWith("m=" + kind + " "))
                isKind = true;
            else if (line.startsWith("m="))
                isKind = false;
            if (isKind) {
                Matcher skipMatch = getFirstIfRegex(line, skipRegex);
                Matcher videoMatch = getFirstIfRegex(line, videoRegex);
                if (skipMatch != null && !allowed.contains(skipMatch.group(2))) {
                    continue;
                } else if (videoMatch != null) {
//                    Log.d(TAG, "SDP Filter Codec: Add Video Match to SDP: " + videoMatchTemp);
                    String videoMatchTemp = line.replace(videoRegex, "$1 " + allowedStr) + "\n";
//                    Log.d(TAG, "SDP Filter Codec: Add Video Match to SDP: " + videoMatchTemp);
                    sdp += videoMatchTemp;
                } else {
//                    Log.d(TAG, "SDP Filter Codec: Add to SDP: " + line);
                    sdp += line + "\n";
                }
            } else
                sdp += line + "\n";
        }
        Log.e(TAG, "SDP:\n" + sdp);
        return sdp;
    }
}
