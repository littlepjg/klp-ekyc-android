package com.kalapa.antocr.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.kalapa.antocr.AntOcr;

public class Common {

    public static void arrangeHeightListView(ListView lv, boolean shouldCutOff) {
        ListAdapter listadp = lv.getAdapter();
        if (listadp != null) {
            int totalHeight = 0;
            for (int i = 0; i < listadp.getCount(); i++) {
                View listItem = listadp.getView(i, null, lv);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = lv.getLayoutParams();
            if (shouldCutOff) {
                params.height = totalHeight - 200;
            } else {
                params.height = totalHeight + (lv.getDividerHeight() * (listadp.getCount() - 1));

            }
            lv.setLayoutParams(params);
            lv.requestLayout();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
