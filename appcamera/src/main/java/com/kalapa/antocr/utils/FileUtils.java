package com.kalapa.antocr.utils;

import android.content.*;
import android.graphics.*;
import android.util.Log;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.io.*;

/**
 * FaceTracker
 * Created by taint on 05/16/2019.
 */
public class FileUtils {
    private static final String TAG = FileUtils.class.getName();
    public static final String cardFileName = "card_image.jpg";
    public static final String tempFileName = "temp.jpg";
    public static final String cardBackFileName = "card_back_image.jpg";
    public static final String faceFileName = "face_image.jpg";
    public static final String faceFileNameCloseEyes = "face_image_close_eyes.jpg";
    public static final String faceFileNameRight = "face_image_right.jpg";
    public static final String faceFileNameLeft = "face_image_left.jpg";

    public static void saveFile(Context context, Bitmap bitmap, String fileName) {

        if (fileName == null || fileName.isEmpty()) return;
        File savedPhoto = new File(context.getCacheDir(), fileName);
        Log.d(TAG, String.format("Image Save File: %s - w:%s - h:%s", fileName, bitmap.getWidth(), bitmap.getHeight()));
        try {
            ImageUtils.ImageSize imgSize = ImageUtils.getInstance().getDropboxIMGSize(savedPhoto);
            Log.d(TAG, String.format("Image Save File: %s - w:%s - h:%s", fileName, imgSize.getWidth(), imgSize.getHeight()));
            FileOutputStream outputStream = new FileOutputStream(savedPhoto.getPath());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            //outputStream.write(capturedImage);
            outputStream.flush();
            outputStream.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    public static File getCardFile(Context context) {
        return new File(context.getCacheDir(), cardFileName);
    }

    public static File getTempFile(Context context) {
        return new File(context.getCacheDir(), tempFileName);
    }


    public static File getFaceFile(Context context) {
        return new File(context.getCacheDir(), faceFileName);
    }

    public static File getFaceFileCloseEyes(Context context) {
        return new File(context.getCacheDir(), faceFileNameCloseEyes);
    }

    public static File getFaceFileRight(Context context) {
        return new File(context.getCacheDir(), faceFileNameRight);
    }

    public static File getFaceFileLeft(Context context) {
        return new File(context.getCacheDir(), faceFileNameLeft);
    }

    public static File getCardBackFile(Context context) {
        return new File(context.getCacheDir(), cardBackFileName);
    }
}
