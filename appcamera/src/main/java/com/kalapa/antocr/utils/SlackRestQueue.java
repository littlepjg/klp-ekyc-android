package com.kalapa.antocr.utils;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class SlackRestQueue extends AsyncTask<String, String, String> {
    String host;
    String message;

    public static final String CUONGVC = "<@UCMP7L5JA>";
    public static final String THIENDN = "<@UCM96SU02>";
    public static final String TUNG = "<@UCTJZQ13R>";
    public static final String DUCNH = "<@UCRUS5HFH>";
    public static final String NAMPTH = "<@UD9Q1304T>";
    public static final String GIANGNT = "<@UL44UJL84>";
    public static final String THANGPV = "<@UNM8VLA2C>";
    public static final String DUYPN = "<@UPLAJAX6U>";

    public static final String SLACK_WEBHOOKS_DEV_TU = "https://hooks.slack.com/services/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"; // dev_tu
    public static final String SLACK_WEBHOOKS_NOTI = "https://hooks.slack.com/services/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"; // noti
    //    private  static final String SLACK_WEBHOOKS_NOTI = "https://hooks.slack.com/services/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"; // dev_tu
    public static final String SLACK_WEBHOOKS_ERROR_NOTI = "https://hooks.slack.com/services/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"; // error

    private Retrofit retrofit;

    public SlackRestQueue(String host, String message) {
        this.host = host;
        this.message = message;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            OkHttpClient client = new OkHttpClient();
            RequestBody formBody = new FormBody.Builder()
                    .add("text", message) // Another sample POST field
                    .build();
            Request request = new Request.Builder()
                    .url(host) // The URL to send the data to
                    .post(formBody)
                    .build();
            Response response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
