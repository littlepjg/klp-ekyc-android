package com.kalapa.antocr;

import android.util.Log;

import com.google.gson.Gson;
import com.kalapa.antocr.model.AntifraudFull;
import com.kalapa.antocr.model.CardInfo;
import com.kalapa.antocr.model.DataVerify;
import com.kalapa.antocr.model.DecisionSummarize;
import com.kalapa.antocr.model.FaceVerifyInfo;
import com.kalapa.antocr.model.GetTokenObj;
import com.kalapa.antocr.model.SessionObject;
import com.kalapa.antocr.model.VerifyOCR;
import com.kalapa.antocr.utils.ImageUtils;

import java.io.File;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Demo_module
 * Created by tung on 06/08/2021.
 */
class ApiService {
    private Retrofit retrofitGateway;
    private final String sessionToken;
    private final String host;
    private static final String TAG = ApiService.class.getName();
    private static final String DATA_TYPE_FRONT = "FRONT";
    private static final String DATA_TYPE_BACK = "BACK";
    private static final String DATA_TYPE_SELFIE = "SELFIE";
    private static final String DATA_TYPE_VERIFY = "VERIFY";
    private static final String DATA_TYPE_LOGIC = "LOGIC";
    private static final String DATA_TYPE_ANTIFRAUD = "ANTIFRAUD";

    public ApiService(String sessionToken, String host) {
        this.sessionToken = sessionToken;
        this.host = host;
        createGateway();
    }

    private OkHttpClient getHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging).build();
    }

    private void createGateway() {
        retrofitGateway = new Retrofit.Builder()
                .baseUrl(host)
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


    Observable<FaceVerifyInfo> faceVerify(File face) {
        try {
            ImageUtils.ImageSize selfieSize = ImageUtils.getInstance().getDropboxIMGSize(face);
            Log.d(TAG, String.format("Selfie Size: w:%s - h:%s ", selfieSize.getWidth(), selfieSize.getHeight()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody requestBodyCardFile = RequestBody.create(face, MediaType.parse("multipart/form-data"));
        MultipartBody.Part facePart = MultipartBody.Part.createFormData("image", face.getName(), requestBodyCardFile);
        IApiService apiService = retrofitGateway.create(IApiService.class);
        RequestBody requestId = RequestBody.create(AntOcr.getSession(), MediaType.parse("multipart/form-data"));
        return apiService.faceVerify(this.sessionToken, requestId, facePart);
    }

    Observable<CardInfo> detectFront(File card) {
        RequestBody requestBodyCardFile = RequestBody.create(card, MediaType.parse("multipart/form-data"));
        MultipartBody.Part cardPart = MultipartBody.Part.createFormData("image", card.getName(), requestBodyCardFile);
        RequestBody requestId = RequestBody.create(AntOcr.getSession(), MediaType.parse("multipart/form-data"));
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.detectFront(this.sessionToken, requestId, cardPart);
    }

    Observable<CardInfo> detectBack(File card) {
        RequestBody requestBodyCardFile = RequestBody.create(card, MediaType.parse("multipart/form-data"));
        MultipartBody.Part cardPart = MultipartBody.Part.createFormData("image", card.getName(), requestBodyCardFile);
        RequestBody requestId = RequestBody.create(AntOcr.getSession(), MediaType.parse("multipart/form-data"));
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.detectBack(this.sessionToken, requestId, cardPart);
    }

    Observable<SessionObject> getSessionToken(String token, String sessionPlan, boolean showAllResult, String uploadType, String callbackUrl) {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//        RequestBody body = RequestBody.create("{\"app_token\":\"" + token + "\"}", JSON);
        GetTokenObj body = new GetTokenObj(token, sessionPlan, showAllResult, uploadType, callbackUrl);
        Log.d(TAG, "Body : " + new Gson().toJson(body));
        return apiService.getSessionToken(body);
    }

    Observable<Object> checkIfSessionExpired() {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.checkIfSessionExpired(this.sessionToken);
    }

    Observable<CardInfo> getFrontIdData() {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.getIdData(sessionToken, DATA_TYPE_FRONT);
    }

    Observable<CardInfo> getBackIdData() {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.getIdData(sessionToken, DATA_TYPE_BACK);
    }

    Observable<FaceVerifyInfo> getSelfieData() {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.getSelfieData(sessionToken);
    }

    Observable<AntifraudFull> getAntifraudData() {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.getAntifraudData(sessionToken);
    }

    Observable<VerifyOCR> getLogicData() {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.getLogicData(sessionToken);
    }

    Observable<DecisionSummarize> getDecisionData() {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.getDecisionData(sessionToken);
    }

    Observable<DataVerify> getVerifyData() {
        IApiService apiService = retrofitGateway.create(IApiService.class);
        return apiService.getVerifyData(sessionToken);
    }


}
